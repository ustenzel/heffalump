module SillyStats
    ( kiv_options, dstat_options, yadda_options
    , SillyStats(..), TwoD(..)
    , abbacounts, print_table, showPValue, accum_stats, gen_stats, requireAtLeast
    ) where

-- Regarding this whole module:  the duplication between functions that
-- compute statistics and functions that compute the labels is ugly.
-- Could the traversals be separated from the computations?

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude
import Bio.Streaming                    ( Stream )
import Bio.Streaming.Vector             ( concatVectors )
import Numeric.SpecFunctions            ( incompleteBeta )
import Options.Applicative

import qualified Bio.Streaming.Prelude              as Q
import qualified Data.Vector                        as V
import qualified Data.Vector.Storable               as U

import Treemix ( merge_inputs_variants, InputConf(..), input_options, input_options_N, VarDensity(..) )

showPValue :: Double -> ShowS
showPValue x | x >= 0.002 = showFFloat (Just 3) x
             | otherwise  = showEFloat (Just 1) x

-- Silly counting statistics.  Commonalities:
--
-- - We count stuff, then compute the ratio of some stuff to other stuff.
-- - We perform block jackknife to estimate standard error.
-- - Blocks are weighted according to the value of the denominator above.
-- - The block size is some constant in genomic coordinates.  It doesn't
--   need to be precise, only "large enough".
-- - Given lots of inputs, we compute more lots of statistics.
--
-- We shall scan a list of variants, apply the counting function to each.
-- Then chop into blocks and sum over blocks; keep this table.  Jackknife
-- is a second pass over the table.  We use 'Double's instead of plain
-- 'Int's, because we want to have fractional counts (instead of saampling
-- one representative count randomly) without confusing the Jackknife
-- downstream.  Also, 52 bits for any actual integer counts is still
-- plenty.

type CountFunction r = U.Vector AlleleCounts -> U.Vector r

data TwoD = TwoD !Double !Double

instance Storable TwoD where
    alignment _ = alignment (0::Double)
    sizeOf    _ = 3 * sizeOf (0::Double)

    poke p (TwoD x y) = pokeElemOff (castPtr p) 0 x >>
                        pokeElemOff (castPtr p) 1 y
    peek p = TwoD <$> peekElemOff (castPtr p) 0
                  <*> peekElemOff (castPtr p) 1

instance Semigroup TwoD where
    TwoD a b <> TwoD a' b' = TwoD (a+a') (b+b')

instance Monoid TwoD where
    mempty = TwoD 0 0
    mappend = (<>)

data SillyStats = SillyStats
    { _silly_count      :: !Double       -- ^ pseudo-count of successes
    , _silly_total      :: !Double       -- ^ pseudo-count of everything
    , _silly_est        :: !Double       -- ^ the point estimate
    , _silly_var        :: !Double       -- ^ variance (from block jackknife)
    , _silly_p          :: !Double }     -- ^ p-value (one-sided beta-test)

gen_stats :: U.Vector TwoD -> V.Vector (U.Vector TwoD) -> [SillyStats]
gen_stats full_counts blockstats = final_vals
  where
    -- We generate the count (k), the total (n), the result (k/n),
    -- the variance estimate (\sigma^2, from Jackknifing)
    final_vals = [ SillyStats k n (2*k/n -1) (4*v)
                              (if n == 0 then 0/0 else pval (k/n) v)
                 | i <- [0 .. U.length full_counts - 1]
                 , let TwoD k n = full_counts U.! i
                 , let v        = blk_jackknife k n $ V.map (U.! i) blockstats ]


    -- p-value.  Use a Beta distribution with a=b=1/8V - 1/2 to match
    -- the variance.  p-value is the CDF evaluated at r (or 1-r,
    -- whichever is <0.5);
    pval r v |  r >= 1   = 0
             |  r <= 0   = 0
             |  r > 0.5  = incompleteBeta ab ab (1-r)
             | otherwise = incompleteBeta ab ab   r
      where ab = recip (8*v) - 0.5


-- Block Jackknife for estimating ratios.  The bias corrected estimator
-- reduces to the plain estimator, so we skip this.  We estimate
-- variance by applying the "Delete-m Jackknife for Unequal m".
--
-- The variance estimator reduces to
-- \[
-- \hat{\sigma}^2 = \frac{1}{G} \sum_{j=1}^G \frac{1}{n-m_j) \left(
--  \frac{k_j^2}{m_j} - 2 \frac{k_j K}{n} + \frac{K^2 m_j}{n^2} \right)
-- \]
--
-- (Pray to FSM that this is actually correct.)
--
-- Arguments are K (sum of the \$k_j\$), N (sum of the \$m_j\$) and the
-- list of pairs of \$(k_j,m_j)\$; result is \$\sigma^2\$.

blk_jackknife :: Double -> Double -> V.Vector TwoD -> Double
blk_jackknife kk nn = divide . V.foldl' step (0 :!: (0::Int))
  where
    divide (s :!: l) = s / fromIntegral l

    step (s :!: l) (TwoD kj mj) | mj == 0   =               s :!: succ l
                                | otherwise = d / (nn-mj) + s :!: succ l
      where
        d = kj*kj / mj - 2 * kj * kk / nn + kk*kk*mj / (nn*nn)

-- --------------- Kayvergence

-- | Produces labels for 'kayvergence' in the same order that statistics
-- are produces.  Input is number of good genomes and labels for
-- individuals, the reference is added internally.
kaylabels :: Int -> [Bytes] -> [(Bytes,Bytes,Bytes)]
kaylabels ngood labels =
    [ ( ref, smp, outg )
    | (iref,ref):goods <- tails $ take (ngood+1) labels'
    , (ioutg,outg) <- goods
    , (ismp,smp) <- labels'
    , ismp /= iref && ismp /= ioutg ]
  where
    labels' = zip [(0::Int) ..] $ "reference" : labels

-- | Counter function that computes all sensible Kayvergence ratios.  It
-- receives the number of "good genomes" appearing at the front of the
-- list of calls.  The reference allele is added at the front, it also
-- counts as good.  For the statistic itself, two good genomes are picked
-- (their order doesn't matter) along with another one (good or bad, can be
-- any one, even further up in the list).
--
-- It then counts the total number of differences between the two good
-- genomes, and the number of those where the bad genome matches the first
-- good genome.  Their ratio is the Kayvergence.
kayvergence :: Int -> CountFunction TwoD
kayvergence ngood callz = U.fromListN nstats
    [ kaycounts (at ref) (at smp) (at outg)
    | ref <- [0 .. ngood-1]             -- pick from ref and good genomes, but not the last
    , outg <- [ref+1..ngood]            -- pick another good genome
    , smp <- [0 .. ntot-1]              -- pick sample
    , smp /= ref && smp /= outg ]       -- must be different
  where
    at 0 = AC 1 0                       -- the reference allele
    at i = callz U.! (i-1)

    -- number of ways to pick two good genomes and one ordinary one
    nstats = ngood * (ngood+1) * (ntot-2) `div` 2
    ntot   = U.length callz + 1         -- number of individuals

    -- Counting for 'kayvergence'.  @aa@ is the number of combinations
    -- that count as shared variation; @aa+bb@ is the number of
    -- combinations that count as variation.  Their ratio will
    -- contribute to our final estimate, but we need to weight it by the
    -- total number of combinations, which is $\prod_i u_i+v_i$.
    kaycounts (AC u1 v1) (AC u2 v2) (AC u3 v3) =
            if nn > 0 then TwoD (aa/nn) ((aa+bb)/nn) else mempty
      where
        aa = fromIntegral $ u1 * v2 * v3 + v1 * u2 * u3
        bb = fromIntegral $ u1 * u2 * v3 + v1 * v2 * u3
        nn = fromIntegral $ (u1 + v1) * (u2 + v2) * (u3 + v3)

kiv_options :: Parser (LIO ())
kiv_options = kiv_main
    <$> input_options_N (long "numgood" <> metavar "NUM" <> help "The first NUM inputs are \"good\" genomes")
    <*> option readOOM  (short 'J' <> long "blocksize" <> metavar "NUM" <> value 5000000 <>
                         showDefaultWith showOOM <> help "Set blocksize for Jackknife to NUM bases")
  where
    kiv_main ic@InputConf{..} conf_blocksize =
        liftIO $
        requireAtLeast [ ( 2, input_noutgroups, "good genomes" )
                       , ( 3, length input_hefs, "genomes" ) ] $
        merge_inputs_variants ic Sparse NoHoles $ \_ref ->
            print_table .
            zipWith fmt1 (kaylabels input_noutgroups $ map fpToSampleName input_hefs) .
            uncurry gen_stats <=<
            accum_stats_ conf_blocksize (kayvergence input_noutgroups)

    fmt1 (rn,sn,cn) (SillyStats k n r v _) =
            [ Left $ "Kiv( " ++ unpack rn ++ "; "
            , Left $ unpack sn ++ "; "
            , Left $ unpack cn
            , Left   " ) = "
            , Right $ showFFloat (Just 0) k "/"
            , Right $ showFFloat (Just 0) n " = "
            , Right $ showFFloat (Just 2) (100 * r) "% ± "
            , Right $ showFFloat (Just 2) (100 * sqrt v) "%" ]



-- --------------- D-Stats

pattersonlbls :: Int -> Int -> [Bytes] -> [(Bytes,Bytes,Bytes,Bytes)]
pattersonlbls nout nref labels =
    [ ( smp, outg, refA, refB )
    | outg <- outlabels
    , refA:refs' <- tails reflabels
    , refB <- refs'
    , smp <- smplabels ]
  where
    outlabels = take nout labels
    reflabels = take nref $ drop nout labels
    smplabels = drop (nout+nref) labels

-- D-stats.  We compute D(X,O;G,H) where X is the sample, O is an
-- outgroup, G,H are from a reference panel.  So, in analogy to the
-- Kayvergence, we pick one from the outgroup, two from the ref panel,
-- and one sample.

pattersons :: Int -> Int -> U.Vector AlleleCounts -> U.Vector TwoD
pattersons nout nref callz = U.fromListN nstats
    [ abbacounts smp outg refA refB
    | outg <- U.toList outcalls
    , refA:refs' <- tails $ U.toList refcalls
    , refB <- refs'
    , smp <- U.toList smpcalls ]
  where
    outcalls = U.take nout callz
    refcalls = U.take nref $ U.drop nout callz
    smpcalls = U.drop (nout+nref) callz

    -- number of ways to pick one outgroup, two references, and one sample
    nstats = U.length outcalls * U.length smpcalls *
             U.length refcalls * (U.length refcalls -1) `div` 2

abbacounts :: AlleleCounts -> AlleleCounts -> AlleleCounts -> AlleleCounts -> TwoD
abbacounts (AC u1 v1) (AC u2 v2) (AC u3 v3) (AC u4 v4) =
        if nn > 0 then TwoD (baba/nn) ((abba+baba)/nn) else mempty
  where
    abba = fromIntegral $ u1 * v2 * v3 * u4 + v1 * u2 * u3 * v4
    baba = fromIntegral $ v1 * u2 * v3 * u4 + u1 * v2 * u3 * v4
    nn   = fromIntegral $ (u1 + v1) * (u2 + v2) * (u3 + v3) * (u4 + v4)


dstat_options :: Parser (LIO ())
dstat_options = dstat_main
    <$> input_options
    <*> option auto    (short 'k' <> long "numrefpanel" <> metavar "NUM" <> value 2 <>
                        showDefault <> help "The next NUM inputs are the reference panel")
    <*> option readOOM (short 'J' <> long "blocksize" <> metavar "NUM" <> value 5000000 <>
                        showDefaultWith showOOM <> help "Set blocksize for Jackknife to NUM bases")
  where
    dstat_main ic@InputConf{..} conf_nrefpanel conf_blocksize =
        liftIO $
        requireAtLeast [ ( 2, input_noutgroups, "outgroups" )
                       , ( 2, conf_nrefpanel, "members in reference panel" )
                       , ( 1, length input_hefs - input_noutgroups - conf_nrefpanel, "samples" )] $
        merge_inputs_variants ic Sparse NoHoles $ \_ref ->
            let labels = pattersonlbls input_noutgroups conf_nrefpanel (map fpToSampleName input_hefs)
            in print_table . zipWith fmt1 labels . uncurry gen_stats
                <=< accum_stats_ conf_blocksize (pattersons input_noutgroups conf_nrefpanel)

    fmt1 (sn,cn,r1,r2) (SillyStats k n r v p) =
            [ Left   "D( "
            , Left $ unpack r1 ++ ", "
            , Left $ unpack r2 ++ "; "
            , Left $ unpack sn ++ ", "
            , Left $ unpack cn
            , Left   " ) = "
            , Right $ showFFloat (Just 0) k "/"
            , Right $ showFFloat (Just 0) n " = "
            , Right $ showFFloat (Just 2) (100 * r) "% ± "
            , Right $ showFFloat (Just 2) (100 * sqrt v) "%, p = "
            , Right $ showPValue p [] ]

-- --------------- Yadda-yadda

yaddalbls :: Int -> Int -> Int -> [Bytes] -> [(Bytes,Bytes,Bytes,Bytes,Bytes)]
yaddalbls nape nout nnea labels =
    [ ( ape, neaA, neaB, smp, afr )
    | ape <- apelabels
    , afr <- afrlabels
    , neaA:neas <- tails nealabels
    , neaB <- neas
    , smp <- smplabels ]
  where
    apelabels = take nape labels
    afrlabels = take nout $ drop nape labels
    nealabels = take nnea $ drop (nout+nape) labels
    smplabels = drop (nape+nout+nnea) labels

-- | Yadda-yadda-counts: We will have to pick one outgroup (C, chimp),
-- two neandertals (N1, N2), one human (H), one human outgroup (Y,
-- yoruba).  Alternatively, we pick two humans and one neandertal, then
-- compute the patterns as (C, H1, H2, N, Y).  The same patterns are
-- counted.  Parameters are the number of true outgroups ('nape',
-- typically the chimp), number of human outgroups ('nout', typically
-- racially pure africans), number of neanderthals ('nnea').
--
-- (Arguably, D-statistics with the chimp as outgroup, two neanderthals,
-- and one human might detect the second admixture while being
-- insensitive to the first.  The same thing could be done with two
-- humans and one neanderthal.  Maybe this is complete bullshit after
-- all.)

yaddayadda :: Int -> Int -> Int -> CountFunction TwoD
yaddayadda nape nout nnea callz = U.fromListN nstats
    [ yaddacounts ape neaA neaB smp afr
    | ape <- U.toList apecalls
    , afr <- U.toList afrcalls
    , neaA:neas <- tails $ U.toList neacalls
    , neaB <- neas
    , smp <- U.toList smpcalls ]
  where
    apecalls = U.take nape callz
    afrcalls = U.take nout $ U.drop nape callz
    neacalls = U.take nnea $ U.drop (nout+nape) callz
    smpcalls = U.drop (nape+nout+nnea) callz

    -- number of ways to pick one outgroup, one african, one sample,
    -- and two neanderthals
    nstats = U.length apecalls * U.length afrcalls * U.length smpcalls
           * U.length neacalls * (U.length neacalls -1) `div` 2

    -- Yadda-yadda:  We compute patterns of the shape (C, N1, N2, H, Y),
    -- then count with a positive sign AADDA, AADDD, ADAAD and with a
    -- negative sign AADAD, ADADA, ADADD.  (Need to do it twice, because
    -- the human reference can be either state.)
    yaddacounts (AC u1 v1) (AC u2 v2) (AC u3 v3) (AC u4 v4) (AC u5 v5) =
            if nn > 0 then TwoD (aa/nn) ((aa+bb)/nn) else mempty
      where
        aadda = u1 * u2 * v3 * v4 * u5  +  v1 * v2 * u3 * u4 * v5
        aaddd = u1 * u2 * v3 * v4 * v5  +  v1 * v2 * u3 * u4 * u5
        adaad = u1 * v2 * u3 * u4 * v5  +  v1 * u2 * v3 * v4 * u5

        aadad = u1 * u2 * v3 * u4 * v5  +  v1 * v2 * u3 * v4 * u5
        adada = u1 * v2 * u3 * v4 * u5  +  v1 * u2 * v3 * u4 * v5
        adadd = u1 * v2 * u3 * v4 * v5  +  v1 * u2 * v3 * u4 * u5

        aa = fromIntegral $ aadda + aaddd + adaad
        bb = fromIntegral $ aadad + adada + adadd
        nn = fromIntegral $ (u1 + v1) * (u2 + v2) * (u3 + v3) * (u4 + v4) * (u5 + v5)

yadda_options :: Parser (LIO ())
yadda_options = yadda_main
    <$> input_options
    <*> option auto    (short 'k' <> long "numafricans" <> metavar "NUM" <> value 1 <>
                        showDefault <> help "The next NUM inputs are africans")
    <*> option auto    (short 'j' <> long "numneandertals" <> metavar "NUM" <> value 2 <>
                        showDefault <> help "The next NUM inputs are neanderthals")
    <*> option readOOM (short 'J' <> long "blocksize" <> metavar "NUM" <> value 5000000 <>
                        showDefaultWith showOOM <> help "Set blocksize for Jackknife to NUM bases")
  where
    yadda_main ic@InputConf{..} conf_nafricans conf_nrefpanel conf_blocksize =
        liftIO $
        requireAtLeast [( 2, input_noutgroups, "apes" )
                       ,( 1, conf_nafricans, "africans" )
                       ,( 2, conf_nrefpanel, "neanderthals" )
                       ,( 1, length input_hefs - input_noutgroups - conf_nafricans - conf_nrefpanel, "samples" )] $
        merge_inputs_variants ic Sparse NoHoles $ \_ref ->
            let labels = yaddalbls input_noutgroups conf_nafricans conf_nrefpanel (map fpToSampleName input_hefs)
            in print_table . zipWith fmt1 labels . uncurry gen_stats
                <=< accum_stats_ conf_blocksize (yaddayadda input_noutgroups conf_nafricans conf_nrefpanel)

    fmt1 (cn,an,n1,n2,sn) (SillyStats k n r v p) =
            [ Left "Y( "
            , Left $ unpack cn ++ "; "
            , Left $ unpack n1 ++ ", "
            , Left $ unpack n2 ++ "; "
            , Left $ unpack sn ++ ", "
            , Left $ unpack an
            , Left   " ) = "
            , Right $ showFFloat (Just 0) k "/"
            , Right $ showFFloat (Just 0) n " = "
            , Right $ showFFloat (Just 2) (100 * r) "% ± "
            , Right $ showFFloat (Just 2) (100 * sqrt v) "%, p = "
            , Right $ showPValue p [] ]


print_table :: [[Either String String]] -> IO ()
print_table tab = putStrLn . unlines $ map (concat . zipWith fmt1 lns) tab
  where
    lns = map (maximum . map (either length length)) $ transpose tab
    fmt1 l (Left  s) = s ++ replicate (l - length s) ' '
    fmt1 l (Right s) =      replicate (l - length s) ' ' ++ s

accum_stats_ :: (Storable r, Monoid r)
             => Int -> CountFunction r -> Stream (Of Variant) IO x
             -> IO ( U.Vector r, V.Vector (U.Vector r) )
accum_stats_ blk_size cfn = fmap Q.fst' . accum_stats blk_size cfn

accum_stats :: (Storable r, Monoid r, PrimMonad m)
            => Int -> CountFunction r -> Stream (Of Variant) m x
            -> m (Of ( U.Vector r, V.Vector (U.Vector r) ) x)
accum_stats blk_size cfn vs0 = do
    blockstats_ :> x <- concatVectors (Q.unfoldr foldBlocks vs0)
    blockstats <- V.unsafeFreeze blockstats_
    let full_counts = V.foldl1' (U.zipWith mappend) blockstats
    return $ (full_counts, blockstats) :> x
  where
    foldBlocks = Q.next >=> \case
        Left    r    -> return $ Left r
        Right (v,vs) -> do x :> r <- foldBlock v (Q.span (near v) vs)
                           return $ Right (V.singleton x,r)

    near v v' = v_chr v == v_chr v' && v_pos v + blk_size > v_pos v'

    foldBlock v = Q.fold (\acc -> U.zipWith mappend acc . cfn . v_calls) (cfn $ v_calls v) id


requireAtLeast :: MonadIO m => [(Int,Int,String)] -> m () -> m ()
requireAtLeast [] k = k
requireAtLeast (( want, have, label ):xs) k
    | want <= have = requireAtLeast xs k
    | otherwise = do liftIO $ hPrintf stderr "Need at least %d %s, got only %d.\n" want label have
                     requireAtLeast xs $ liftIO (hPutStr stderr "Nothing to do.\n")

