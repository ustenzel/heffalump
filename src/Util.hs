module Util
    ( parseFasta
    , FastaSeq(..)

    , InternalError(..)
    , DataError(..)
    , PebkacError(..)
    ) where

import Bio.Prelude               hiding ( fail )
import Bio.Streaming                    ( Stream, ByteStream, inspect, wrap, effect, yields )

import qualified Bio.Streaming.Bytes             as S
import qualified Data.ByteString                 as B

data FastaSeq m r = FastaSeq !Bytes (ByteStream m r) deriving Functor

-- | Parsing a FastA file results in pairs of sequence names and
-- sequences.  The sequences are still text with their case preserved.
-- This assumes that the character '>' does not show up anywhere except
-- to introduce a header and that downstream can deal with whitespace
-- (especially line breaks).  Annoying as the use of
-- streaming-bytestring is here, it completely prevents otherwise
-- unmanageable memory leaks.

parseFasta :: Monad m => ByteStream m r -> Stream (FastaSeq m) m r
parseFasta = go . ignoreBody . S.lines
  where
    go :: Monad m => Stream (ByteStream m) m r -> Stream (FastaSeq m) m r
    go = lift . inspect >=> go'

    go' :: Monad m => Either r (ByteStream m (Stream (ByteStream m) m r)) -> Stream (FastaSeq m) m r
    go' (Left    r) = pure r
    go' (Right hdr) = do
                h :> ls <- lift $ S.toStrict hdr
                let name = B.drop 1 $ B.takeWhile (not . is_space_w8) h
                yields (FastaSeq name (S.concat (takeBody ls))) >>= go

    is_space_w8 :: Word8 -> Bool
    is_space_w8 x = (x .&. 0x7F) <= 32

    -- Take lines as long as they don't start with ">"
    takeBody :: Monad m => Stream (ByteStream m) m r -> Stream (ByteStream m) m (Stream (ByteStream m) m r)
    takeBody = lift . inspect >=> \case
        -- empty input stream:  return an empty stream that returns an
        -- empty stream.
        Left r -> pure (pure r)     -- ?!
        -- inspect first line...
        Right line -> lift (S.uncons line) >>= \case
            -- empty line:  return it and continue
            Left     s'                    -> yields (pure s') >>= takeBody

            -- header line:  return empty stream that returns an empty
            -- stream that starts with the restored line.
            Right (c,line') | c == c2w '>' -> pure (wrap (S.cons c line'))

            -- body line:  return a stream that begins with it, recurse
            -- on its tail
                            | otherwise    -> yields (S.cons c line') >>= takeBody

    ignoreBody :: Monad m => Stream (ByteStream m) m r -> Stream (ByteStream m) m r
    ignoreBody = lift . inspect >=> \case
        -- empty input stream:  return an empty stream
        Left r -> pure r
        -- inspect first line...
        Right line -> lift (S.uncons line) >>= \case
            -- empty line:  continue
            Left      s'                   -> ignoreBody s'

            -- header line: return a stream that starts with the restored line.
            Right (c,line') | c == c2w '>' -> wrap (S.cons c line')

            -- body line:  continue
                            | otherwise    -> ignoreBody $ effect (S.effects line')


-- | Errors that should never happen and indicate a bug.
data InternalError = InternalError String deriving (Typeable, Show)

-- | Errors that happen because of malformed input.
data DataError = DataError (Maybe FilePath) String deriving (Typeable, Show)

-- | Errors that happen because the user did something nonsensical.
data PebkacError = PebkacError String deriving (Typeable, Show)

instance Exception InternalError where
    displayException (InternalError msg) =
        "Internal error: " ++ msg ++ ". " ++
        "Please report this bug at https://bitbucket.org/ustenzel/heffalump/issues"

instance Exception DataError where
    displayException (DataError src msg) = "Error in " ++ descr src ++ ": " ++ msg
      where
        descr  Nothing   = "input"
        descr (Just "-") = "stdin"
        descr (Just  x ) = show x

instance Exception PebkacError where
    displayException (PebkacError msg) = msg

