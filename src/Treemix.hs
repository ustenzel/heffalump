module Treemix
    ( treemix_options
    , InputConf(..)
    , input_options
    , input_options_N
    , VarDensity(..)
    , density_option
    , merge_inputs_variants
    ) where

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude
import Bio.Streaming                    ( Stream, withOutputFile, distribute )
import Bio.Streaming.Bytes              ( concatBuilders, gzip, hPut, toByteStream )
import Control.Monad.Log
import Control.Monad.Trans.Reader
import Data.ByteString.Builder          ( Builder, intDec, char7, byteString )
import Options.Applicative
import Text.Regex.Posix                 ( Regex, makeRegex, makeRegexM, matchTest )

import qualified Bio.Streaming.Prelude           as Q
import qualified Data.ByteString.Char8           as B
import qualified Data.HashMap.Strict             as H
import qualified Data.Vector.Storable            as V

import Bed
import Util

data InputConf = InputConf
    { input_hefs       :: [FilePath]
    , input_reference  :: Maybe FilePath
    , input_noutgroups :: Int
    , input_transv     :: Bool
    , input_nosplit    :: Bool
    , input_nocpg      :: Bool
    , input_chroms     :: Maybe Regex
    , input_regions    :: Maybe FilePath }

input_options :: Parser InputConf
input_options = input_options_N $
    long "numoutgroups" <> metavar "NUM" <> help "The first NUM individuals are outgroups"

input_options_N :: Mod OptionFields Int -> Parser InputConf
input_options_N nopt = InputConf
    <$> many (strArgument (metavar "HEF-FILE" <> help "Input files will be merged"))
    <*> optional (strOption (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
    <*> option auto (short 'n' <> value 1 <> showDefault <> nopt)
    <*> switch (short 't' <> long "transversions" <> help "Restrict to transversion variants")
    <*> switch (short 'b' <> long "biallelic-only" <> help "Discard, don't split, polyallelic sites")
    <*> switch (long "ignore-cpg" <> help "Ignore GpG sites (according to reference)")
    <*> optional
           (option regex (short 'c' <> long "chromosomes" <> metavar "REGEX" <> help "Restrict to a subset of chromosomes")
        <|> flag' (re "^(chr)?[0-9]+[a-z]?$") (short 'a' <> long "autosomes" <> help "Restrict to autosomes")
        <|> flag' (re "^(chr)?X$") (short 'x' <> long "x-chromosome" <> help "Restrict to X chromsome")
        <|> flag' (re "^(chr)?Y$") (short 'y' <> long "y-chromosome" <> help "Restrict to Y chromsome"))
    <*> optional (strOption (short 'R' <> long "regions" <> metavar "BED-FILE" <> help "Restrict to regions in BED-FILE"))
  where
    re :: String -> Regex
    re = makeRegex

    regex :: ReadM Regex
    regex = do s <- str ; makeRegexM (s :: String)


data VarDensity = Sparse | Dense

density_option :: Parser VarDensity
density_option = flag Sparse Dense (short 'D' <> long "dense" <> help "Output invariant sites, too")

merge_inputs_variants :: (MonadIO m, MonadMask m)
                      => InputConf -> VarDensity -> MaxHole -> (TwoBitFile -> Stream (Of Variant) m () -> m r) -> m r
merge_inputs_variants InputConf{..} dense maxhole k =
        decodeManyRef input_reference input_hefs $ \ref inps -> do
            let ref_names = tbf_chrnames ref
            region_filter <- liftIO $ readBed input_regions ref_names
            k ref $ Q.filter (\Variant{..} ->
                        -- samples (not outgroups) must show ref and alt allele at least once
                        let I2 nref nalt = V.foldl' addAC (I2 0 0) $ V.drop input_noutgroups v_calls
                            is_ti = not input_transv || isTransversion v_alt
                        in is_ti && nref /= 0 && nalt /= 0)
                  $ maybe id filterWithBed region_filter
                  $ (if input_nocpg then filterCpG ref else id)
                  $ chrom_filter ref_names input_chroms
                  $ case (dense, input_nosplit) of (Sparse, False) -> expandVars
                                                   (Sparse,  True) -> expandSimpleVars
                                                   (Dense,  False) -> expandVarsDense
                                                   (Dense,   True) -> expandSimpleVarsDense
                  $ addRef maxhole ref
                  $ mergeLumps (NumOutgroups input_noutgroups) inps


treemix_options :: Parser (LIO ())
treemix_options = treemix_main
    <$> input_options
    <*> strOption (value "-" <> short 'o' <> long "output" <> metavar "FILE" <> help "Write output to FILE (.tmx.gz)")
    <*> optional (strOption (short 'i' <> long "individuals" <> metavar "FILE" <> help "Read individuals from FILE (.ind)"))
  where
    treemix_main :: InputConf -> FilePath -> Maybe FilePath -> LIO ()
    treemix_main conf conf_output conf_indivs
      = do
        -- We read and merge all the HEF files (shell trickery is suggested
        -- to assemble the horrible command line).  We use the optional IND
        -- file to map them to populations.

        let hefs = input_hefs conf
        (pops, npops, popixs) <- case conf_indivs of
            Just fp -> do popmap <- readIndFile <$> liftIO (B.readFile fp)
                          return . toSymtab $ map (lookupHef popmap) hefs
            Nothing -> return (map fpToSampleName hefs, length hefs, V.enumFromN 0 (length hefs))

        withOutputFile conf_output $ \hdl ->
            merge_inputs_variants conf Sparse NoHoles $ \_refs -> do
                hPut hdl . gzip . toByteStream .
                    (<>) (foldr (\a k -> byteString a <> char7 ' ' <> k) (char7 '\n') pops) <=<
                    streamBuilders . Q.map
                        (\Variant{..} -> V.foldr (\(I2 a b) k -> intDec a <> "," <> intDec b <> " " <> k) "\n" $
                                         V.accumulate_ addAC (V.replicate npops $ I2 0 0) popixs v_calls)

streamBuilders :: Stream (Of Builder) LIO () -> LIO Builder
streamBuilders s = Logged $ ReaderT $ pure . concatBuilders . runReaderT (runLogged (distribute s))

chrom_filter :: Monad m => [Bytes] -> Maybe Regex -> Stream (Of Variant) m r -> Stream (Of Variant) m r
chrom_filter _chroms  Nothing  = id
chrom_filter  chroms (Just re) = go [ i | (i,chrom) <- zip [0..] chroms, matchTest re chrom ]
  where
    go [    ] = lift . Q.effects
    go (c:cs) = go1
      where
        go1 = lift . Q.next >=> \case
            Left r -> pure r
            Right (v,vs)
                | c < v_chr v -> go cs (Q.cons v vs)
                | c > v_chr v -> go1 vs
                | otherwise   -> v `Q.cons` go1 vs

-- | Reads an individual file.  Returns a map from individual to pop
-- population number.
readIndFile :: B.ByteString -> [(B.ByteString, B.ByteString)]
readIndFile = mapMaybe (get1 . B.words) . filter (not . B.isPrefixOf "#") . B.lines
  where
    get1 (x:_:y:_) = Just (x,y)
    get1     _     = Nothing

-- | Finds the population for a file.  If there is exactly one
-- individual whose name is a prefix of the basename of the filepath,
-- the result is its associated population.
lookupHef :: [(B.ByteString, B.ByteString)] -> FilePath -> B.ByteString
lookupHef assocs fp = case matches of
    (_ ,y) : [    ]               -> y
    (x1,y) : (x2,_) : _ | x1 > x2 -> y
    _:_:_ -> throw . PebkacError $ "Multiple populations match " ++ show (fpToSampleName fp)
    [   ] -> throw . PebkacError $ "No population matches " ++ show (fpToSampleName fp)
  where
    matches = sortBy (flip compare) . map (first B.length) .
              filter (\(x,_) -> x `B.isPrefixOf` fpToSampleName fp) $ assocs

-- | Takes a list of stuff, returns the list without dups, its length,
-- and a vector of symbols (integer shorthands for the stuff).
toSymtab :: [B.ByteString] -> ([B.ByteString], Int, V.Vector Int)
toSymtab = go [] [] H.empty 0
  where
    go ss is _ n [    ] = (reverse ss, n, V.reverse $ V.fromList is)
    go ss is h n (x:xs) = case H.lookup x h of
            Just  i -> go    ss  (i:is)               h        n  xs
            Nothing -> go (x:ss) (n:is) (H.insert x n h) (succ n) xs

