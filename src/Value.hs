{-# LANGUAGE CPP, Rank2Types, ExistentialQuantification #-}
module Value where

import Bio.Prelude
import Text.Regex.Posix                         ( (=~) )
import qualified Data.ByteString.Builder        as B
import qualified Data.ByteString.Char8          as C

-- ^ The 'Value' type and associated functions and instances.  Supports
-- a dynamic language with integers, floating point numbers, string, and
-- a null value.

data Value = Null | Int !Int | Double !Double | String !Bytes

accum_values :: Foldable f => Value -> f Value -> Value
accum_values v0 = either id id . foldl' step (Left v0)
  where
    -- foldl1' is curiously missing from 'Foldable'
    step (Left   _) !v = Right v
    step (Right !u) !v = Right $! plus_values u v

-- not a semi group, because this isn't associative
plus_values :: Value -> Value -> Value
plus_values  Null              x  = x
plus_values  x          Null      = x
plus_values (Int    a) (Int    b) = Int    $ a + b
plus_values (Int    a) (Double b) = Double $ fromIntegral a + b
plus_values (Int    a) (String b) = String $ fromString (show a) <> b
plus_values (Double a) (Int    b) = Double $ a + fromIntegral b
plus_values (Double a) (Double b) = Double $ a + b
plus_values (Double a) (String b) = String $ fromString (show a) <> b
plus_values (String a) (Int    b) = String $ a <> fromString (show b)
plus_values (String a) (Double b) = String $ a <> fromString (show b)
plus_values (String a) (String b) = String $ a <> b

instance Eq Value where
    a == b = compare a b == EQ

instance Ord Value where
    compare Null  Null      = EQ
    compare Null (Int    y) = compare 0 y
    compare Null (Double y) = compare 0 y
    compare Null (String y) = compare "" y

    compare (Int x)  Null      = compare x 0
    compare (Int x) (Int    y) = compare x y
    compare (Int x) (Double y) = compare (fromIntegral x) y
    compare (Int x) (String y) = compare (Int x) (to_num y)

    compare (Double x)  Null      = compare x 0
    compare (Double x) (Int    y) = compare x (fromIntegral y)
    compare (Double x) (Double y) = compare x y
    compare (Double x) (String y) = compare (Double x) (to_num y)

    compare (String x)  Null      = compare x ""
    compare (String x) (Int    y) = compare (to_num x) (Int y)
    compare (String x) (Double y) = compare (to_num x) (Double y)
    compare (String x) (String y) = compare x y


showValue :: Value -> B.Builder
showValue  Null      = B.string8 "??\n"
showValue (Int    a) = B.intDec     a <> B.char7 '\n'
showValue (Double a) = B.doubleDec  a <> B.char7 '\n'
showValue (String a) = B.byteString a

-- | What's truish for filtering purposes?  The only falsish values are
-- Null and numeric zeroes, everything else, notably the empty string,
-- is truish.  (Empty strings can be returned from bed file lookups and
-- should be truish to make testing for inclusion easy.)
to_bool :: Value -> Bool
to_bool  Null      = False
to_bool (Int    0) = False
to_bool (Double 0) = False
to_bool  _         = True

from_bool :: Bool -> Value
from_bool = bool (Int 0) (Int 1)

notV :: Value -> Value
notV = from_bool . not . to_bool

negV :: Value -> Value
negV  Null      = Null
negV (Int    x) = Int (negate x)
negV (Double x) = Double (negate x)
negV (String x) = negV $ to_num x

andV, orV :: Value -> Value -> Value
andV a b = bool a b (to_bool a)
orV  a b = bool b a (to_bool a)


to_num :: Bytes -> Value
to_num s =
  case C.readInt s of
    Nothing                                                     -> Int 0
    Just (x,t) | C.all isSpace t                                -> Int x
    Just (x,t) ->
      case C.uncons t of
        Just (y0,ys0) | y0 == '.' ->
          case C.readInt ys0 of
            Just (part, zs) | C.all isSpace zs && part >= 0     -> Double frac
              where base = 10 ^ (C.length ys0 - C.length zs)
                    frac = fromIntegral x + (fromIntegral part / base)
            _                                                   -> Int 0
        _                                                       -> Int 0


-- lift arithmetic operation to 'Value's
arith :: (Int -> Int -> Int) -> (Double -> Double -> Double) -> Value -> Value -> Value
arith f g (String a) y = arith f g (to_num a) y
arith f g x (String b) = arith f g x (to_num b)

arith _ _  Null      Null = Null
arith f _ (Int    a) Null = Int $ f a 0
arith _ g (Double a) Null = Double $ g a 0

arith f _  Null      (Int b) = Int $ f 0 b
arith f _ (Int    a) (Int b) = Int $ f a b
arith _ g (Double a) (Int b) = Double $ g a (fromIntegral b)

arith _ g  Null      (Double b) = Double $ g 0 b
arith _ g (Int    a) (Double b) = Double $ g (fromIntegral a) b
arith _ g (Double a) (Double b) = Double $ g a b


catV, sepV, mulV, addV, subV, divV :: Value -> Value -> Value
mulV = arith (*) (*)
addV = arith (+) (+)
subV = arith (-) (-)
catV a b = String C.empty `plus_values` a `plus_values` b
sepV a b = a `plus_values` String " " `plus_values` b

-- division promotes 'Int' to Double' where necessary
divV (String a) y = divV (to_num a) y
divV x (String b) = divV x (to_num b)

divV  Null _ = Null
divV  _ Null = Null

divV  _         (Int    0) = Null
divV  _         (Double 0) = Null

divV (Int    a) (Int    b) = Double $ fromIntegral a / fromIntegral b
divV (Double a) (Int    b) = Double $              a / fromIntegral b
divV (Int    a) (Double b) = Double $ fromIntegral a /              b
divV (Double a) (Double b) = Double $              a /              b

matV :: Value -> Value -> Value
matV (String x) (String y) = from_bool $ x =~ y
matV _ _ = Null

nmtV :: Value -> Value -> Value
nmtV (String x) (String y) = from_bool . not $ x =~ y
nmtV _ _ = Null

-- | The type of expressions that may depend on the environment, but
-- don't depend on a record.  These can always be promoted to 'Fold's
-- that make sense, even if there is no input data.
data CFun e a = CFun { _getNilConst :: a, _getConst :: e -> a }

-- | The type of expressions that depend on only one record.  These can
-- be used in filter expressions or in accumulations.  They carry a
-- neutral value along so they can be promoted into a 'Fold' that feels
-- natural.  (Accumulation is always done using (<>).)
data Func e a b = Func { _getNil :: b, getFunc :: e -> a -> b }

instance Functor (Func e a) where
    fmap f (Func a b)              = Func (f a) ((.) f . b)

instance Applicative (Func e a) where
    pure a                         = Func a (const $ const a)
    Func f g <*> Func u v          = Func (f u) (\e x -> g e x (v e x))
#if MIN_VERSION_base(4,10,0)
    liftA2 f (Func a b) (Func u v) = Func (f a u) (\e x -> f (b e x) (v e x))
#endif

const_to_func :: CFun e b -> Func e a b
const_to_func (CFun n f) = Func n (const . f)


int :: (e -> a -> Int) -> Func e a Value
int f = Func (Int 0) ((.) Int . f)

string :: (e -> a -> Bytes) -> Func e a Value
string f = Func (String C.empty) ((.) String . f)

-- Converts to 'Int', then folds with 'mappend'.  So folding over a
-- boolean expression amounts to counting.
boolv :: (e -> a -> Bool) -> Func e a Value
boolv f = Func (Int 0) ((.) from_bool . f)


data Fold a b = forall x . Fold (x -> x -> x) (a -> x) x (x -> b)

instance Functor (Fold a) where
    fmap f (Fold comb step ini fin) = Fold comb step ini (f . fin)

instance Applicative (Fold a) where
    pure b = Fold const (const ()) () (const b)

    Fold comb1 step1 ini1 fin1 <*> Fold comb2 step2 ini2 fin2 = Fold comb step ini fin
      where
        comb (x :!: y) (u :!: v) = comb1 x u :!: comb2 y v
        ini                      = ini1 :!: ini2
        step a                   = step1 a :!: step2 a
        fin  (x :!: y)           = fin1 x (fin2 y)

#if MIN_VERSION_base(4,10,0)
    liftA2 f (Fold comb1 step1 ini1 fin1) (Fold comb2 step2 ini2 fin2) = Fold comb step ini fin
      where
        comb (x :!: y) (u :!: v) = comb1 x u :!: comb2 y v
        ini                      = ini1 :!: ini2
        step  a                  = step1 a :!: step2 a
        fin  (x :!: y)           = f (fin1 x) (fin2 y)
#endif

