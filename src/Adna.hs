module Adna ( adna_options ) where

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude               hiding ( Ns )
import Bio.Streaming                    ( distribute )
import Control.Monad.Trans.State.Strict ( StateT, evalStateT, state, get, put )
import Options.Applicative
import SillyStats
import System.Random                    ( StdGen, getStdGen, randomR )

import qualified Bio.Streaming          as Q
import qualified Bio.Streaming.Prelude  as Q
import qualified Data.Vector            as V
import qualified Data.Vector.Storable   as U

-- Presumed deamination rate.  With the rate as high as 40% in the first
-- position and falling off by 1/3 per base, we expect 0.6 Cs deaminated
-- at the 5' end and another 0.6 at the 3' end, for a total of 1.2 per
-- read.  Assuming a read length of 35, that's roughly one in 30.
rate :: Int
rate = 30

deaminate :: Monad m => TwoBitFile -> Q.Stream (Of Lump) m r -> Q.Stream (Of Lump) (StateT StdGen m) r
deaminate genome strm = normalizeLump $ case map (`tbc_fwd_seq` 0) $ toList $ tbf_chroms genome of
    r:rs -> evalStateT (distribute (deaminate' r strm)) rs
    [  ] -> lift2 (Q.effects strm)

lift2 :: (Monad m, Monad (t0 m), MonadTrans t0, MonadTrans t1) => m a -> t1 (t0 m) a
lift2 = lift . lift

lift3 :: (Monad m, Monad (t0 m), Monad (t1 (t0 m)), MonadTrans t0, MonadTrans t1, MonadTrans t2) => m a -> t2 (t1 (t0 m)) a
lift3 = lift . lift . lift

deaminate' :: Monad m => TwoBitSequence -> Q.Stream (Of Lump) m r -> Q.Stream (Of Lump) (StateT [TwoBitSequence] (StateT StdGen m)) r
deaminate' rs = lift3 . Q.next >=> either pure deaminate'1
  where
    -- The easy part: These can be passed through.  (Strictly speaking,
    -- the insertions could change, but we don't have any in the SGDP,
    -- do we don't care.)
    deaminate'1 (Ns   n, l) = Ns   n `Q.cons` deaminate' (dropRS n rs) l
    deaminate'1 (Del1 n, l) = Del1 n `Q.cons` deaminate' rs l
    deaminate'1 (Del2 n, l) = Del2 n `Q.cons` deaminate' rs l
    deaminate'1 (DelH n, l) = DelH n `Q.cons` deaminate' rs l

    deaminate'1 (Ins1 s, l) = Ins1 s `Q.cons` deaminate' rs l
    deaminate'1 (Ins2 s, l) = Ins2 s `Q.cons` deaminate' rs l
    deaminate'1 (InsH s, l) = InsH s `Q.cons` deaminate' rs l

    deaminate'1 (Break,  l) = lift get >>= \case
            (x:xs) -> lift  (put xs)      >>        Break `Q.cons` deaminate' x l
            ([  ]) -> lift3 (Q.effects l) >>= \r -> Break `Q.cons` return r

    -- Given the right reference, these can generate Trans1 or RefTrans.
    -- In principle, Trans2 would be possible, though relatively
    -- unlikely, and we don't bother with that.
    deaminate'1 (Eqs1 n, l) = deam_eqs n l Eqs1 Trans1   (Eqs1 1)
    deaminate'1 (Eqs2 n, l) = deam_eqs n l Eqs2 RefTrans RefTrans

    -- What remains is the variant codes.  Depending on the reference
    -- allele, these can change or even disappear.  We draw a random
    -- number up to twice the rate, if it's 0, we deaminate' the first
    -- (or only) allele, if it's 1 the second.  Else we do nothing.
    deaminate'1 (var, l) = case unconsRS rs of
        Just (ref,rs') | isKnown ref -> do
            i <- lift2 . state $ randomR (0,2*rate-1)
            case i of 0 -> deam1 ref var
                      1 -> deam2 ref var
                      _ -> var
                 `Q.cons` deaminate' rs' l
        _ -> var `Q.cons` deaminate' rs  l

    deam1 r v = toVars r $ case appVars r v of
        (1, b) -> (0, b)
        (3, b) -> (2, b)
        (a, b) -> (a, b)

    deam2 r v = toVars r $ case appVars r v of
        (a, 1) -> (a, 0)
        (a, 3) -> (a, 2)
        (a, b) -> (a, b)


    -- This is awful.  Though necessary, it probably belongs in a more
    -- central location?
    toVars :: Nuc2b -> (Word8,Word8) -> Lump
    toVars (N2b r) (a,b) = case (xor r a, xor r b) of
        (0,0) -> Eqs2 1
        (1,0) -> RefTrans
        (2,0) -> RefCompl
        (3,0) -> RefTCompl
        (0,1) -> RefTrans
        (1,1) -> Trans2
        (2,1) -> TransCompl
        (3,1) -> TransTCompl
        (0,2) -> RefCompl
        (1,2) -> TransCompl
        (2,2) -> Compl2
        (3,2) -> ComplTCompl
        (0,3) -> RefTCompl
        (1,3) -> TransTCompl
        (2,3) -> ComplTCompl
        (3,3) -> TCompl2

        (1,_) -> Trans1
        (2,_) -> Compl1
        (3,_) -> TCompl1
        (_,_) -> Eqs1 1

    appVars :: Nuc2b -> Lump -> (Word8, Word8)
    appVars (N2b r) RefTrans    = (r `xor` 0, r `xor` 1)
    appVars (N2b r) Trans2      = (r `xor` 1, r `xor` 1)
    appVars (N2b r) RefCompl    = (r `xor` 0, r `xor` 2)
    appVars (N2b r) TransCompl  = (r `xor` 1, r `xor` 2)
    appVars (N2b r) Compl2      = (r `xor` 2, r `xor` 2)
    appVars (N2b r) RefTCompl   = (r `xor` 0, r `xor` 3)
    appVars (N2b r) TransTCompl = (r `xor` 1, r `xor` 3)
    appVars (N2b r) ComplTCompl = (r `xor` 2, r `xor` 3)
    appVars (N2b r) TCompl2     = (r `xor` 3, r `xor` 3)

    appVars (N2b r) Trans1      = (r `xor` 1, 255)
    appVars (N2b r) Compl1      = (r `xor` 2, 255)
    appVars (N2b r) TCompl1     = (r `xor` 3, 255)

    appVars (N2b _) _           = (255, 255)


    deam_eqs n l eqs trans1 trans2 = do
        -- Get waiting time to deamination event and the affected allele.
        (d,s) <- lift2 . state $ randomBitGeom (recip $ fromIntegral rate)

        -- beyond the end, do nothing
        if True --d >= n
            then eqs n `Q.cons` deaminate' (dropRS n rs) l
            -- else check reference allele and deaminate' if possible
            else case unconsRS (dropRS d rs) of
                -- this shouldn't happen
                Nothing -> eqs n `Q.cons` deaminate' RefEnd l

                Just (N2b r, rs') ->
                    let v = if r == 1 || r == 3 then bool trans1 trans2 s else eqs 1
                    in eqs d `Q.cons` (v `Q.cons` deaminate' rs' (eqs (n-d-1) `Q.cons` l))


randomBitGeom :: Double -> StdGen -> ((Int, Bool), StdGen)
randomBitGeom p g = case randomR (0,1) g of
    (q,g') | q  <  0.5 -> ((floor $ log (2*q  ) / log (1 - p), False), g')
           | otherwise -> ((floor $ log (2*q-1) / log (1 - p), True),  g')


type Buf a m r = (Q.Stream (Of a) m r, Maybe (Q.Stream (Of a) m ()), Maybe (Q.Stream (Of a) m ()))

-- Hm.  Ugly.  Duplicates a stream, but not the effects.  Involves some
-- buffering.
copyIO :: MonadIO m => Q.Stream (Of a) m r -> m (Q.Stream (Of a) m r, Q.Stream (Of a) m r)
copyIO s0 = do v <- liftIO $ newIORef (s0, Nothing, Nothing)
               return (go_left v, go_right v)
  where
    go_left  = go id
    go_right = go (\(x,y,z) -> (x,z,y))

    go :: MonadIO m => ( Buf a m r -> Buf a m r ) -> IORef (Buf a m r) -> Q.Stream (Of a) m r
    go f v = do (s, l, r) <- liftIO $ f <$> readIORef v
                case l of
                  Nothing -> lift (Q.next s) >>= \case
                     Left z -> do liftIO $ writeIORef v $ f (pure z, Nothing, r)
                                  pure z

                     Right (h,t) -> do liftIO $ writeIORef v $ f (t, Nothing, Just $ fromMaybe (pure ()) r >> Q.yield h)
                                       h `Q.cons` go f v

                  Just ls -> do liftIO $ writeIORef v $ f (s, Nothing, r)
                                ls >> go f v

progress :: MonadLog m => Q.Stream (Of Variant) m r -> Q.Stream (Of Variant) m r
progress = go 0 0
  where
    go rs po = lift . Q.next >=> \case
      Left r -> r <$ lift (logString_ [])
      Right (v,vs) -> do
            when (rs /= v_chr v || po + 10000000 <= v_pos v) $
                lift $ logString_ $ shows (v_chr v) ":" ++ show (v_pos v)
            v `Q.cons` go (v_chr v) (v_pos v) vs


adna_options :: Parser (LIO ())
adna_options = adna_main
    <$> many (strArgument (metavar "HEF-FILE"))
    <*> optional (strOption (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
    <*> option auto (value 1 <> short 'n' <> long "numoutgroups" <> metavar "NUM" <>
                     showDefault <> help "The first NUM inputs are outgroups")
    <*> option readOOM (value 5000000 <> short 'J' <> long "blocksize" <> metavar "NUM" <>
                        showDefaultWith showOOM <> help "Set blocksize for Jackknife to NUM bases")
  where
    adna_main hefs conf_reference conf_noutgroups conf_blocksize =
        requireAtLeast [( 1, conf_noutgroups, "outgroups" )
                       ,( 1, length hefs - conf_noutgroups, "samples" )] $
        decodeManyRef conf_reference hefs $ \ref inps -> do

            let (ogs,sms) = V.splitAt conf_noutgroups inps
            let ogs' = V.map (Q.hoist lift) ogs
            sms' <- V.concatMap (\(x,y) -> V.fromList [Q.hoist lift x, deaminate ref y]) <$> V.mapM copyIO sms

            gen <- liftIO $ getStdGen
            stats_cpg :> stats_t :> stats :> _ <-
                        fmap (Q.mapOf (uncurry gen_stats))
                        $ accum_stats conf_blocksize (bastard_d conf_noutgroups)
                        $ filterCpG ref

                        $ fmap (Q.mapOf (uncurry gen_stats))
                        $ accum_stats conf_blocksize (bastard_d conf_noutgroups)
                        $ Q.filter (isTransversion . v_alt)
                        $ Q.copy

                        $ fmap (Q.mapOf (uncurry gen_stats))
                        $ accum_stats conf_blocksize (bastard_d conf_noutgroups)
                        $ Q.copy

                        $ progress
                        $ flip evalStateT gen . distribute
                        $ expandVars
                        $ mergeLumps (NumOutgroups conf_noutgroups) (ogs' V.++ sms')

            let fmt1 (sn,cn,r1,r2) (SillyStats k n r v p) =
                        [ Left $ "D( "
                        , Left $ unpack r1 ++ ", "
                        , Left $ unpack r2 ++ "; "
                        , Left $ unpack sn ++ ", "
                        , Left $ unpack cn
                        , Left   " ) = "
                        , Right $ showFFloat (Just 0) (k - n/2) "/"
                        , Right $ showFFloat (Just 0) n " = "
                        , Right $ showFFloat (Just 2) (100 * r) "% ± "
                        , Right $ showFFloat (Just 2) (100 * sqrt v) "%, p = "
                        , Right $ showPValue p [] ]

            let labels = bastard_labels conf_noutgroups (map fpToSampleName hefs)
            liftIO $ do
                putStrLn "plain D statistics"
                print_table $ zipWith fmt1 labels stats

                putStrLn "restricted to transversions"
                print_table $ zipWith fmt1 labels stats_t

                putStrLn "ignoring CpG motifs"
                print_table $ zipWith fmt1 labels stats_cpg


bastard_labels :: Int -> [Bytes] -> [(Bytes,Bytes,Bytes,Bytes)]
bastard_labels nout labels =
    [ ( smpX, outg, smpY, smpY<>"'" )
    | outg <- outlabels
    , smpX <- smplabels
    , smpY <- smplabels ]
  where
    (outlabels,smplabels) = splitAt nout labels


-- D-stats.  The input stream has a bunch of outgroups and a bunch of
-- samples, with the samples duplicated (once pristine, once
-- deaminated).  We pick one outgroup O, one sample X, and one sample Y;
-- and compute D(O,X,Y,Y') where Y' is the deaminated version of Y.
--
-- We compute D(X,O;G,H) where X is a reference, O is an
-- outgroup, G,H are from a reference panel.  So, in analogy to the
-- Kayvergence, we pick one from the outgroup, two from the ref panel,
-- and one sample.

bastard_d :: Int -> U.Vector AlleleCounts -> U.Vector TwoD
bastard_d nout callz = U.fromListN nstats
    [ abbacounts smpX outg smpY smpY'
    | outg <- U.toList outcalls
    , smpX:_:_ {-smps'-} <- tails2 $ U.toList smpcalls
    , smpY:smpY':_ <- tails2 $ U.toList smpcalls {- smps' -} ]
  where
    (outcalls,smpcalls) = U.splitAt nout callz
    nstats = U.length outcalls * U.length smpcalls * U.length smpcalls `div` 4
    -- nstats = U.length outcalls * U.length smpcalls * (U.length smpcalls -2) `div` 8

    tails2 xs@(_:_:ys) = xs : tails2 ys
    tails2       _     = []
