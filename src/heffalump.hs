{-# LANGUAGE TemplateHaskell #-}
import Bio.Heffa.Genome
import Bio.Heffa.Legacy
import Bio.Heffa.Lump
import Bio.Prelude
import Bio.Streaming                    ( Stream, ByteStream, inspect, wrap, streamInput, withOutputFile )
import Bio.Util.Git                     ( giFullVersion, gitInfo )
import Paths_heffalump                  ( version )
import Options.Applicative

import qualified Bio.Streaming.Bytes             as S
import qualified Bio.Streaming.Prelude           as Q
import qualified Data.ByteString.Builder         as B
import qualified Data.ByteString.Lazy.Char8      as L
import qualified Data.IntMap                     as I

import Adna
import Bamin
import BcfScan
import Eigenstrat
import Emf
import Query
import ShameMS
import SillyStats
import Treemix
import Util
import Vcf
import VcfOut

options :: Parser (LIO ())
options =
    hsubparser ( commandGroup "Import commands:"
        <> metavar "Import-Command"
        <> command "hetfa"         (info   hetfa_options (progDesc "Import a hetfa file"))
        <> command "maf"           (info     maf_options (progDesc "Import one genome from maf files"))
        <> command "emf"           (info     emf_options (progDesc "Import one genome from emf (Compara)"))
        <> command "bcfin" (info (xcf_options "BCF-FILE" readBcf) (progDesc "Import one genome from bcf files"))
        <> command "vcfin" (info (xcf_options "VCF-FILE" readVcf)
            ( progDesc "Import one genome from vcf files" <> footer
              "This mode of operation is total crap and should only be \
              \used on VCF files that are sufficiently broken that \
              \bcftools won't convert them to BCF anymore." ))
        <> command "bamin"         (info     bam_options
            ( progDesc "Import crudely from bam files" <> footer
              "Using a proper genotype caller is much preferred, even on \
              \ancient dna.  This command exists merely to be able to \
              \reproduce an established, but silly analysis method." ))
        <> command "legacy"        (info  legacy_options (progDesc "Upgrade legacy hef files")))
    <|>
    hsubparser ( commandGroup "Export commands:"
        <> metavar "Export-Command"
        <> command "patch"         (info   patch_options (progDesc "Make a hetfa file by patching the reference"))
        <> command "treemix"       (info treemix_options (progDesc "Merge heffalumps into Treemix format"))
        <> command "eigenstrat"  (info (eigen_options  writeEigenstrat) (progDesc "Merge heffalumps into Eigenstrat format"))
        <> command "ancestrymap" (info (eigen_options writeAncestrymap) (progDesc "Merge heffalumps into Ancestrymap format"))
        <> command "vcfexport"     (info  vcfout_options (progDesc "Merge heffalumps into vcf format"))
        <> command "pseudo_ms"     (info   shame_options (progDesc "Merge heffalumps into pseudo-ms format"))
        <> command "bsfs"          (info    bsfs_options (progDesc "Compute the block-SFS"))
        <> command "dumplump"      (info    dump_options (progDesc "(debugging aid)")))
    <|>
    hsubparser ( commandGroup "Genome (2bit) manipulation:"
        <> metavar "Genome-Command"
        <> command "twobitinfo"    (info  tbinfo_options (progDesc "List reference sequences"))
        <> command "twobittofa"    (info  tbtofa_options (progDesc "Extract Fasta from 2bit"))
        <> command "fatotwobit"    (info  fatotb_options (progDesc "Convert Fasta to 2bit"))
        <> command "vcftotwobit"   (info vcftotb_options (progDesc "Extract reference from dense VCF to 2bit"))
        <> command "transcriptome" (info  tbxome_options (progDesc "Transform GTF annotation into a transcriptome")))
    <|>
    hsubparser ( commandGroup "Analysis commands:"
        <> metavar "Analysis-Command"
        <> command "query"         (info   query_options (progDesc "Perform magic"))
        <> command "kayvergence"   (info     kiv_options (progDesc "Compute Kayvergence ratios"))
        <> command "dstatistics"   (info   dstat_options (progDesc "Compute Patterson's D"))
        <> command "yaddayadda"    (info   yadda_options (progDesc "(professionals only)"))
        <> command "adna"          (info    adna_options (progDesc "(professionals only)")))


main :: IO ()
main = execWithParser_ options (Just version) (giFullVersion $$(gitInfo))
            (progDesc "Stores genotype calls compactly and processes them in simple ways." <>
             header "Compact genotype storage" <> fullDesc) id

hetfa_options :: Parser (LIO ())
hetfa_options = hetfa_main
    <$> strArgument (value "-" <>                     metavar "FILE" <> help "Read sample from FILE (.hetfa)")
    <*> strOption   (short 'o' <> long "output"    <> metavar "FILE" <> help "Write output to FILE (.hef)")
    <*> strOption   (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)")
 where
    hetfa_main :: FilePath -> FilePath -> FilePath -> LIO ()
    hetfa_main conf_imp_sample conf_imp_output conf_imp_reference = do
        refs <- liftIO $ openTwoBit conf_imp_reference
        streamInput conf_imp_sample $
            S.writeFile conf_imp_output . S.gzip .
                importHetfa refs . parseFasta . S.gunzip


-- | Import Hetfa by diffing it against a reference.  We must read the
-- Hetfa in the order of the file (Fasta doesn't have an index), but
-- write it in the order of the reference.  So we parse a chromosome at
-- a time, sort the diffs in memory and dump it at the end.
--
-- Chromosomes in input that don't have a match in the reference are
-- silently ignored.  Chromsomes that don't show up in the input are
-- emitted as empty sequences.  Only if no sequence matches do we
-- produce an error message.
importHetfa :: (MonadIO m, MonadLog m, MonadThrow m) => TwoBitFile -> Stream (FastaSeq m) m r -> ByteStream m r
importHetfa ref smps = S.mwrap $ do
    (map1,r) <- fold_1 I.empty smps

    when (I.null map1) . throw . PebkacError $
            "Found only unexpected sequences.  Is this the right reference?"

    return $ do S.toByteStream $ encodeHeader ref
                forM_ (tbf_chroms ref) $ \c ->
                    S.fromLazy $ unpackLump $ I.findWithDefault noLump (tbc_index c) map1
                pure r
  where
    enc2 :: (MonadIO m, MonadThrow m) => TwoBitSequence -> ByteStream m r -> m (Of PackedLump r)
    enc2 rs sq = encodeLumpToMem $ wrap (Break :> diff2 rs sq)

    fold_1 :: (MonadIO m, MonadLog m, MonadThrow m) => I.IntMap PackedLump -> Stream (FastaSeq m) m r -> m (I.IntMap PackedLump, r)
    fold_1 !acc s = inspect s >>= \case
        Left r -> return (acc,r)
        Right (FastaSeq nm sq) -> case findChrom nm ref of
            Nothing      -> do logStringLn $ "skipping unknown scaffold " ++ show nm
                               S.effects sq >>= fold_1 acc
            Just TBC{..} -> do
                lump :> s' <- enc2 (tbc_fwd_seq 0) sq
                logStringLn $ shows nm " (" ++ shows tbc_index ") : " ++ shows (L.length $ unpackLump lump) " bytes."
                fold_1 (I.insert tbc_index lump acc) s'

maf_options :: Parser (LIO ())
maf_options = maf_main
    <$> many (strArgument (metavar "MAF-FILE"))
    <*> strOption (short 'o' <> long "output"         <> metavar "FILE" <> help "Write output to FILE (.hef)")
    <*> strOption (short 'r' <> long "reference"      <> metavar "FILE" <> help "Read reference from FILE (.2bit)")
    <*> strOption (short 'R' <> long "ref-species"    <> metavar "NAME" <> help "Set reference species to NAME")
    <*> strOption (short 'S' <> long "sample-species" <> metavar "NAME" <> help "Set sample species to NAME")
  where
    maf_main :: [FilePath] -> FilePath -> FilePath -> String -> String -> LIO ()
    maf_main maffs maf_output maf_reference ref_species smp_species = do
        ref <- liftIO $ openTwoBit maf_reference
        let go g f = streamInput f $ parseMaf (Just f) (fromString ref_species, fromString smp_species) ref g . S.gunzip
        S.writeFile maf_output . S.gzip . encodeGenome ref =<<
            foldM go emptyGenome (if null maffs then ["-"] else maffs)


patch_options :: Parser (LIO ())
patch_options = patch_main
    <$> strArgument (metavar "HEF-FILE")
    <*> strOption (value "-" <> short 'o' <> long "output"    <> metavar "FILE" <> help "Write output to FILE (.hetfa)")
    <*> optional (strOption    (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
    <*> option auto            (short 'w' <> long "width"     <> metavar  "NUM" <> help "Set width of FastA to NUM (50)"
                                          <> value 50 <> showDefault)
  where
    patch_main :: FilePath -> FilePath -> Maybe FilePath -> Int -> LIO ()
    patch_main conf_patch_sample conf_patch_output conf_patch_reference conf_patch_width = liftIO $ do
        withBinaryFile conf_patch_sample ReadMode $ \hdli -> do
            (mref, raw) <- getRefPath $ S.gunzip $ S.hGetContents hdli
            ref <- openTwoBit $ fromMaybe (error "no reference") $ mplus conf_patch_reference mref

            withOutputFile conf_patch_output $ \hdl ->
                B.hPutBuilder hdl $ S.concatBuilders $
                patchFasta conf_patch_width (toList $ tbf_chroms ref) $
                decode (Just ref) (Just conf_patch_sample) raw

    patchFasta :: Int -> [TwoBitChromosome] -> Stream (Of Lump) IO r -> Stream (Of B.Builder) IO r
    patchFasta wd (TBC{..}:tbss) p = do Q.yield $ B.char7 '>' <> B.byteString tbc_name <> B.char7 '\n'
                                        buildFasta wd (patch (tbc_fwd_seq 0) p) >>= patchFasta wd tbss
    patchFasta  _   _            p = lift $ Q.effects p


dump_options :: Parser (LIO ())
dump_options = dumplump_main
    <$> strArgument (metavar "HEF-FILE")
    <*> optional (strOption (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
  where
    dumplump_main inf ref = liftIO $ do
        rs <- mapM openTwoBit ref
        withBinaryFile inf ReadMode $ debugLump . decode rs (Just inf) . S.gunzip . S.hGetContents

