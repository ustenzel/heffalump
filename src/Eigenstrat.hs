module Eigenstrat ( eigen_options, writeEigenstrat, writeAncestrymap ) where

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude
import Bio.Streaming                            ( withOutputFile )
import Options.Applicative

import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString                as B
import qualified Data.ByteString.Builder        as B
import qualified Data.ByteString.Internal       as B
import qualified Data.Vector.Generic            as V
import qualified Data.Vector.Storable           as U

import Treemix ( merge_inputs_variants, input_options, InputConf(..), VarDensity(..) )

-- ^ The Eigenstrat and Ancestrymap exporters.  Includes generation of
-- meaningless names and hash functions.

-- Nickhash for strings.
nick_hashit :: Bytes -> Word32
nick_hashit = B.foldl (\h c -> 23 * h + fromIntegral c) 0

-- Nickhash for vectors of strings.
nick_hasharr :: Foldable v => v Bytes -> Word32
nick_hasharr = foldl (\h s -> 17 * h `xor` nick_hashit s) 0

-- Nickhash for the names of SNPs in a stream.
nick_hashstream :: Monad m => (a -> Word32) -> Q.Stream (Of a) m r -> m (Of Word32 r)
nick_hashstream f = Q.fold (\h s -> 17 * h `xor` f s) 0 id

mkname :: Variant -> Word64
mkname Variant{..} = interleave (shiftL v_chr 2 + fromIntegral (unV2b v_alt)) v_pos
  where
    interleave 0 0 = 0
    interleave x y = fromIntegral (y .&. 0xff) .|.
                     fromIntegral (shiftL (x .&. 0xff) 8) .|.
                     shiftL (interleave (shiftR x 8) (shiftR y 8)) 16


encName :: Word64 -> B.Builder
encName 0 = mempty
encName u = B.word8 (B.index name_chars (fromIntegral $ u .&. 31)) <> encName (u `shiftR` 5)

hashSnp :: Word64 -> Word32
hashSnp = enc 0
  where
    enc h 0 = h
    enc h u = enc (23 * h + fromIntegral (B.index name_chars (fromIntegral $ u .&. 31))) (u `shiftR` 5)

name_chars :: Bytes
name_chars = "0123456789ABCDEFGHKLMNPQRSTUWXYZ"

eigen_options :: Sink LIO () -> Parser (LIO ())
eigen_options output = eigen_main
    <$> input_options
    <*> strOption (short 'o' <> long "output" <> metavar "FILE" <> help "Write output to FILE.geno and FILE.snp")
  where
    eigen_main conf_input conf_output =
        merge_inputs_variants conf_input Sparse (MaxHole 49) $ \_refs ->
        output conf_output (map fpToSampleName $ input_hefs conf_input)

type Sink m r = FilePath -> [Bytes] -> Q.Stream (Q.Of Variant) m r -> m r

-- | The Eigenstrat exporter.  Eigenstrat is one character per genotype.
-- This is simple and practical if you want to post-process the data
-- with text tools, but wasteful of space.
writeEigenstrat :: (MonadIO m, MonadMask m) => Sink m r
writeEigenstrat oname _ vars =
    withOutputFile (oname ++ ".snp") $ \hsnp ->
    withOutputFile (oname ++ ".geno") $ \hgeno ->
    flip Q.mapM_ vars $ \v -> liftIO $ do
        B.hPutBuilder hgeno $ U.foldr ((<>) . B.char7 . enc) (B.char7 '\n') (v_calls v)
        putSnp hsnp v
  where
    enc (AC 0 0) = '9'
    enc (AC _ 0) = '2'
    enc (AC 0 _) = '0'
    enc (AC _ _) = '1'

putSnp :: Handle -> Variant -> IO ()
putSnp hsnp v@Variant{..} =
    B.hPutBuilder hsnp $
        -- 1st column is SNP name
        encName (mkname v) <> B.char7 '\t'
        -- "2nd column is chromosome.  X chromosome is encoded as 23.
        -- Also, Y is encoded as 24, mtDNA is encoded as 90, ..."
        <> B.intDec (if v_chr == 24 then 90 else v_chr + 1)
        -- "3rd column is genetic position (in Morgans).
        -- If unknown, ok to set to 0.0"
        <> "\t0.0\t"
        -- "4th column is physical position (in bases)"
        <> B.intDec (v_pos+1) <> B.char7 '\t'
        -- "Optional 5th and 6th columns are reference and variant alleles"
        <> B.char7 (toRefCode v_ref) <> B.char7 '\t'
        <> B.char7 (toAltCode v_alt v_ref) <> B.char7 '\n'



-- | The Ancestrymap exporter, which uses less space than Eigenstrat.
-- It starts with a header record, which contains text and is then
-- padded to the record size as defined below:
-- printf("GENO %d %d %x %x", n_individuals, n_snps, individual_hash, snp_hash);
--
-- That's of course retarded, because we can't know n_snps or snp_hash
-- before we generate the snp file.  So all we can do is reserve space
-- and overwrite the header later.  Therefore, this stuff can never be
-- piped. m(
--
-- To generate individual_hash, we apply nick_hasharr to the vector of
-- individual names; to generate snp_hash we would apply nick_hasharr to
-- the vector of SNP identifiers.  (In reality, the streaming variant of
-- nick_hasharr is fused with nick_hashit and mkname.)
--
-- The remainder is binary:  one record per snp, each record uses 2 bits
-- per individual, MSB used first, padded to a full byte, padded to at
-- least 48 bytes.  That, too, is retarded, because it means the stupid
-- Eigenstrat format is still more compact, as long as you have fewer
-- than 48 individuals.  m(

writeAncestrymap :: (MonadIO m, MonadMask m) => Sink m r
writeAncestrymap oname inds vars =
    withOutputFile (oname ++ ".geno")           $ \hgeno ->
    withOutputFile (oname ++ ".snp")            $ \hsnp  -> do
        liftIO $ B.hPut hgeno $ B.replicate rsize 0        -- space for later
        nsnp :> shash :> r <- Q.mapM_ (\v -> liftIO $ do B.hPut hgeno =<< store rsize (v_calls v)
                                                         putSnp hsnp v) $
                              Q.length $ Q.copy $
                              nick_hashstream (hashSnp . mkname) $ Q.copy vars

        liftIO $ hSeek hgeno AbsoluteSeek 0
        liftIO $ hPrintf hgeno "GENO %d %d %x %x\n\SUB" (length inds) nsnp (nick_hasharr inds) shash
        return r
  where
    !rsize = div (length inds + 3) 4 `max` 48

store :: Int -> U.Vector AlleleCounts -> IO Bytes
store s vs =
    B.create (max s (U.length vs `div` 4)) $ \p ->
        flip V.imapM_ vs $ \i ac -> do
            x <- peekElemOff p (i `div` 4)
            let y = 6 - 2 * (i `mod` 4)
                z = x .&. complement (3 `shiftL` y) .|. enc ac `shiftL` y
            pokeElemOff p (i `div` 4) z
  where
    enc (AC 0 0) = 3
    enc (AC _ 0) = 2
    enc (AC 0 _) = 0
    enc (AC _ _) = 1

