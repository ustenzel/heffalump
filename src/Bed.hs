module Bed ( Bed(..), readBed, parseBed, filterRefWithBed, filterWithBed ) where

import Bio.Heffa.Genome
import Bio.Prelude
import Bio.Streaming                        ( Stream, ByteStream )
import Bio.Streaming.Vector                 ( concatVectors )

import qualified Bio.Streaming.Bytes            as S
import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString.Char8          as C
import qualified Data.HashMap.Strict            as M
import qualified Data.Vector.Algorithms.Intro   as U ( sort )
import qualified Data.Vector.Unboxed            as U


-- | A parsed bed file.  We care only about regions and ignore names,
-- strand, annotations.  Each region is represented as chromosome index,
-- start and end of half-open zero based interval.  Regions are sorted.
newtype Bed = Bed ( U.Vector (Int32,Int32,Int32) )

readBed :: Maybe FilePath -> [ Bytes ] -> IO (Maybe Bed)
readBed  Nothing     _ = return Nothing
readBed (Just fp) refs = fmap Just $ withBinaryFile fp ReadMode $ parseBed refs . S.hGetContents

parseBed :: [ Bytes ] -> ByteStream IO r -> IO Bed
parseBed chrms raw = do
    v :> _ <- concatVectors . Q.map parse1 . S.lines' . S.gunzip $ raw
    U.sort v
    Bed <$> U.unsafeFreeze v
  where
    parse1 :: Bytes -> U.Vector (Int32, Int32, Int32)
    parse1 ln = maybe U.empty U.singleton $ do
                   sq:frm:tho:_ <- Just $ C.words ln
                   ci <- findIndex ((==) sq) chrms
                   (start,"") <- C.readInt frm
                   (end,"") <- C.readInt tho
                   return (fromIntegral ci, fromIntegral start, fromIntegral end)

filterWithBed :: Monad m => Bed -> Stream (Of Variant) m r -> Stream (Of Variant) m r
filterWithBed (Bed vau) = go (U.toList vau)
  where
    go [                 ] = lift . Q.effects >=> pure
    go ((ch, ps, pe) : rs) = lift . Q.next >=> \case
        Left    r    -> pure r
        Right (v,vs)
            -- variant before interval, drop the variant
            | (v_chr v, v_pos v) <  (fromIntegral ch, fromIntegral ps) -> go ((ch,ps,pe):rs) vs

            -- variant after interval, drop the interval
            | (v_chr v, v_pos v) >= (fromIntegral ch, fromIntegral pe) -> go rs (Q.cons v vs)

            -- must be a good variant
            | otherwise                                                -> v `Q.cons` go ((ch,ps,pe):rs) vs


-- | Filters a reference sequence through a bed file.  References can
-- already contain holes, which are taken care of correctly when
-- splitting a genome into blocks.  To filter, we simply create
-- additional holes.
--
-- XXX  This is currently broken for the reverse-complemented sequences.

filterRefWithBed :: Bed -> TwoBitFile -> TwoBitFile
filterRefWithBed (Bed vau) rss = rss { tbf_chroms = new_seqs
                                     , tbf_chrmap = M.fromList $ map (tbc_name &&& id) $ toList new_seqs }
  where
    new_seqs = fmap conv1 $ tbf_chroms rss

    conv1 rs = rs { tbc_fwd_seq = \i -> go 0 (tbc_fwd_seq rs i) . U.toList .
                                        U.drop (search (tbc_index rs)) .
                                        U.take (search (succ (tbc_index rs))) $ vau
                  , tbc_rev_seq = error "filterRefWithBed: not implemented" }

    search i = search' (fromIntegral i) 0 (U.length vau)

    search' i u v | u == v              = u
                  | vau U.! m < (i,0,0) = search'  i (m+1) v
                  | otherwise           = search'  i   u   m
      where m = div (u+v) 2

    go :: Int32 -> TwoBitSequence -> [(Int32,Int32,Int32)] -> TwoBitSequence
    go !_ rs [            ] = manyNs (lengthRS rs) RefEnd
    go !p rs ((_,u,v) : is)
        | p > u     = error "filterRefWithBed: shouldn't happen"
        | otherwise = manyNs (fromIntegral $ u-p) $
                      splitMapRS (fromIntegral $ v-u) (dropRS (fromIntegral $ u-p) rs) $
                      flip (go v) is

    -- Smart constructor to normalize away zero-length holes and to
    -- coalesce consecutive holes.
    manyNs n rs@(SomeSeq msk fp o l rs')
        | n == 0            =  rs
        | isHardMasked msk  =  SomeSeq        msk fp (o - fromIntegral n) (n+l) rs'
        | otherwise         =  SomeSeq hardMasked fp (o - fromIntegral n)   n   rs
    manyNs _     RefEnd     =  RefEnd

