module BcfScan ( readBcf, decodeBcf ) where

-- ^ Minimalistic BCF reader.  We only care about the genotype of the
-- first individual.  Should allow for a handful of shortcuts...
--
-- This assumes that GT is the first field encodes for each sample.  If
-- that assumption is violated, you probably get an incomprehensible and
-- misleading error message.  Doing this right just isn't worth the
-- increase in code complexity.

import Bio.Heffa.Genome                         ( TwoBitFile(..), findChrom, TwoBitChromosome(..) )
import Bio.Prelude
import Bio.Streaming                            ( Stream, ByteStream, streamInput )
import Foreign.C.Types                          ( CChar )
import Vcf                                      ( RawVariant(..) )
import Util                                     ( DataError(..) )

import qualified Bio.Streaming.Bytes            as S
import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString                as B
import qualified Data.ByteString.Char8          as C
import qualified Data.ByteString.Internal       as B
import qualified Data.ByteString.Unsafe         as B
import qualified Data.Vector.Unboxed            as V

readBcf :: (MonadIO m, MonadLog m, MonadMask m) => TwoBitFile -> FilePath -> (Stream (Of RawVariant) m () -> m r) -> m r
readBcf ref fp k = streamInput fp $ k . decodeBcf (Just fp) ref . S.gunzip

-- Parses the contigs declared in the header, but skips over everything
-- else.  This fails if GT is not the first individual field that's
-- encoded, but that should always be the case.
decodeBcf :: (MonadIO m, MonadLog m) => Maybe FilePath -> TwoBitFile -> ByteStream m r -> Stream (Of RawVariant) m r
decodeBcf fp ref = lift . S.splitAt' 9 >=> parse_hdr
  where
    parse_hdr (hdr :> rest)
        | "BCF\2" == B.take 4 hdr
            = do let l_text = slow_word32 5 hdr
                 txt :> body <- lift $ S.splitAt' l_text rest
                 lift (S.unconsChunk body) >>= \case
                    Left r -> pure r
                    Right (s,ss) -> do tab <- lift $ mkSymTab ref txt
                                       getvars fp tab ss s
        | otherwise = liftIO . throwIO $ DataError fp "not a BCFv2 file"

    slow_word32 o s =     fromIntegral (B.index s $ o+0)
                      .|. fromIntegral (B.index s $ o+1) `shiftL`  8
                      .|. fromIntegral (B.index s $ o+2) `shiftL` 16
                      .|. fromIntegral (B.index s $ o+3) `shiftL` 24


getvars :: MonadIO m => Maybe FilePath -> V.Vector Int -> ByteStream m r -> Bytes -> Stream (Of RawVariant) m r
getvars fp !tab strs !str = go 0
  where
    go !off
        | B.length str < 32 + off = lift (S.unconsChunk strs) >>= \case
                                        Right (s,ss) -> getvars fp tab ss (B.append (B.drop off str) s)
                                        Left r | B.length str == off -> pure r
                                               | otherwise           -> liftIO . throwIO $ DataError fp "Short record."
        | otherwise = do
            (!l_shared, !l_indiv) <- liftIO . B.unsafeUseAsCString str $ \p ->
                                            (,) <$> peek32 p off <*> peek32 p (off+4)
            let !l_tot = l_shared + l_indiv + 8

            if B.length str < fromIntegral l_tot + off
                -- This looks kinda unsafe, but I think we should never
                -- hit the end here on properly formatted files.
                then do (hd,tl) <- either (const . liftIO . throwIO $ DataError fp "Short record.") return
                                   =<< lift (S.unconsChunk strs)
                        getvars fp tab tl (B.append (B.drop off str) hd)

                else do !v1 <- liftIO $ B.unsafeUseAsCString str $ \p0 -> do
                                      let !p = plusPtr p0 off
                                      !refid    <- fromIntegral        <$> peek32 p  8
                                      !rv_pos   <- fromIntegral . succ <$> peek32 p 12
                                      !n_allls  <- (`shiftR` 16)       <$> peek32 p 24
                                      !rv_vars  <- get_als fp n_allls (plusPtr p 32)
                                      !rv_gt    <- get_gts fp         (plusPtr p (fromIntegral l_shared + 8))
                                      return $! RawVariant{ rv_chrom = tab V.! refid, rv_fp = fp, .. }
                        v1 `Q.cons` go (fromIntegral l_tot + off)


-- skip over variant ID, then get alleles
get_als :: Maybe FilePath -> Word32 -> Ptr CChar -> IO B.ByteString
get_als fp n !p = do !k1 <- peek8 p 0
                     case k1 of
                        0xF7 -> do !k2 <- peek8 p 1
                                   case k2 of
                                        0x01 -> peek8  p 2 >>= kont . (+3) . fromIntegral  -- should be plenty
                                        0x02 -> peek16 p 2 >>= kont . (+4) . fromIntegral  -- but isn't  :-(
                                        0x03 -> peek32 p 2 >>= kont . (+6) . fromIntegral  -- not even close  :,-(
                                        x -> throwIO $ DataError fp $ "Huh? " ++ show x
                        tp | tp .&. 0xF == 7 -> kont (1 + fromIntegral (tp `shiftR` 4))
                        _                    -> throw $ DataError fp "string expected"
  where
    kont !sk = let !p' = plusPtr p sk in get_als' fp NoFrags 0 n p'


data Frags = Frag !(Ptr CChar) !Int Frags | NoFrags

get_als' :: Maybe FilePath -> Frags -> Int -> Word32 -> Ptr CChar -> IO B.ByteString
get_als'  _ !acc !l 0 !_ = B.createUptoN l $ cpfrags acc l
  where
    cpfrags (Frag ps ln fs) !o !p = do forM_ [0..ln-1] $ \i -> do
                                            x <- peekByteOff ps i
                                            pokeByteOff p (o-ln-1+i) (x::Word8)
                                       pokeByteOff p (o-1) (c2w ',')
                                       cpfrags fs (o-ln-1) p
    cpfrags  NoFrags         _ !p = comma2tab p 0 (l-1)

    comma2tab !p !i !e
        | i == e    = return e
        | otherwise = do x <- peekByteOff p i
                         if x == c2w ',' then pokeByteOff p i (c2w '\t') >> return e
                                         else comma2tab p (i+1) e


get_als' fp !acc !l n !p = peek8 p 0 >>= \case
                                    0xF7 -> peek8 p 1 >>= \case
                                                    0x01 -> peek8  p 2 >>= kont 3 . fromIntegral -- should be plenty
                                                    0x02 -> peek16 p 2 >>= kont 4 . fromIntegral -- but isn't  :-(
                                                    0x03 -> peek32 p 2 >>= kont 6 . fromIntegral -- not even close  :,-(
                                                    x -> throwIO $ DataError fp $ "Huh? " ++ show x
                                    tp | tp .&. 0xF == 7 -> kont 1 (fromIntegral $ tp `shiftR` 4)
                                    _                    -> throwIO $ DataError fp "string expected"
  where
    kont !sk !ln = get_als' fp (Frag (plusPtr p sk) ln acc) (l+ln+1) (n-1) (plusPtr p (sk+ln))


peek8 :: Ptr a -> Int -> IO Word8
peek8 = peekByteOff

peek16 :: Ptr a -> Int -> IO Word16
peek16 = peekByteOff

peek32 :: Ptr a -> Int -> IO Word32
peek32 = peekByteOff

get_gts :: Maybe FilePath -> Ptr CChar -> IO Word16
get_gts fp p = do !ks <- peek8 p 0 >>= \case 1 -> return 2; 2 -> return 3; 3 -> return 5
                                             _ -> throwIO $ DataError fp "wrong data type for GT field"

                  -- we support haploid and diploid, and Word8 and Word16
                  peek8 p ks >>= \case
                    0x11 -> (.|.) 0xFF00 . fromIntegral <$> peek8  p (ks+1)
                    0x12 -> (.|.) 0xFF00                <$> peek16 p (ks+1)

                    0x21 -> do !x <- fromIntegral <$> peek8 p (ks+1)
                               !y <- fromIntegral <$> peek8 p (ks+2)
                               return $! y `shiftL` 8 .|. x

                    0x22 -> do !x <- peek16 p (ks+1)
                               !y <- peek16 p (ks+3)
                               return $! y `shiftL` 8 .|. x

                    b    -> throwIO $ DataError fp $ "only haploid or diploid calls are supported " ++ showHex b []

mkSymTab :: MonadLog m => TwoBitFile -> Bytes -> m (V.Vector Int)
mkSymTab ref = liftM (V.fromList . catMaybes) . mapM parse . C.lines
  where
    parse l = case C.splitAt (C.length key) l of
        (u,v) -> case findChrom c ref of
            _ | u /= key -> pure Nothing
            Just TBC{..} -> Just tbc_index <$ logStringLn (shows c " maps to " ++ shows tbc_name " (" ++ shows tbc_index ").")
            Nothing      -> Just      (-1) <$ logStringLn (shows c " skipped.")
          where c = C.takeWhile (/=',') v
    key = "##contig=<ID="

