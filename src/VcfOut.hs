module VcfOut ( vcfout_options, writeVcf ) where

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude
import Options.Applicative

import qualified Bio.Streaming.Prelude           as Q
import qualified Data.ByteString.Builder         as B
import qualified Data.Vector.Storable            as U

import Treemix ( merge_inputs_variants, input_hefs, input_options, density_option )

vcfout_options :: Parser (LIO ())
vcfout_options = vcfout_main <$> input_options <*> density_option
  where
    vcfout_main conf_input density =
        merge_inputs_variants conf_input density (MaxHole 49) $ \refs ->
        writeVcf (fmap tbc_name $ tbf_chroms refs) (map fpToSampleName $ input_hefs conf_input) stdout

writeVcf :: MonadIO m => Array Bytes -> [Bytes] -> Handle -> Q.Stream (Q.Of Variant) m r -> m r
writeVcf chrs smps hdl vars = do
        liftIO $ B.hPutBuilder hdl $
                   "##fileformat=VCFv4.1\n" <>
                   "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" <>
                   "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT" <>
                   foldMap ((B.char7 '\t' <>) . B.byteString) smps <>
                   B.char7 '\n'

        flip Q.mapM_ vars $ \Variant{..} ->
                liftIO $ B.hPutBuilder hdl $
                    B.byteString (indexArray chrs v_chr) <> B.char8 '\t' <>
                    B.intDec (v_pos+1) <> B.string8 "\t.\t" <>
                    B.char8 (toRefCode v_ref) <> B.char8 '\t' <>
                    B.char8 (toAltCode v_alt v_ref) <> B.string8 "\t.\t.\t.\tGT" <>
                    U.foldr ((<>) . gts) mempty v_calls <>
                    B.char8 '\n'
  where
    gts :: AlleleCounts -> B.Builder
    gts (AC 0 0) = "\t./."      -- N
    gts (AC 1 0) = "\t0"        -- 1x ref
    gts (AC 0 1) = "\t1"        -- 1x alt
    gts (AC _ 0) = "\t0/0"      -- >1x ref
    gts (AC 0 _) = "\t1/1"      -- >1x alt
    gts (AC _ _) = "\t0/1"      -- ref+alt

