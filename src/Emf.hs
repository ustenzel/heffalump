module Emf ( emf_options
           , Genome
           , encodeGenome
           , emptyGenome
           , parseMaf
           , EmfCollision(..)
           , EmfDuplication(..)
           , EmfLengthMismatch(..)
           , EmfOverlap(..)
           ) where

-- ^ Getting variant calls from EMF file (Compara EPO whole genome
-- alignments).  EMF contains a list of blocks, and each block aligns
-- contiguous stretches of genomes; sometimes multiple parts of one
-- genome appear in the same block.
--
-- To scan for variants, we compute a difference between the first
-- column and every other column.  Each column gets mapped to some
-- species:  another ape or one of the human-ape common ancestors.
-- (Note that the human-gorilla ancestor is usually, but not necessarily
-- also the ancestor of the chimp!)  Multiple columns can correspond to
-- the same species, and we only call variants for a given species if at
-- least one column maps to it and all such columns agree.


import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude               hiding ( compl )
import Bio.Streaming                    ( ByteStream, Stream, inspect, streamInput )
import Data.ByteString.Builder          ( Builder, word8, lazyByteString )
import Options.Applicative
import System.Directory                 ( getDirectoryContents, doesDirectoryExist )
import System.FilePath                  ( (</>) )

import qualified Bio.Streaming.Bytes                as S
import qualified Bio.Streaming.Prelude              as Q
import qualified Data.ByteString.Char8              as B
import qualified Data.ByteString.Lazy               as L
import qualified Data.IntMap.Strict                 as I
import qualified Text.Megaparsec                    as P ( Parsec, takeWhileP, oneOf, parse )
import qualified Text.Megaparsec.Byte               as P ( char, string )
import qualified Text.Megaparsec.Byte.Lexer         as P ( float )

import Util

-- | We get disjointed blocks, and they are not guaranteed to be in order.
-- (There is an important use case where they are guaranteed to be
-- jumbled.)  So we need an intermediate, compact representation of the
-- fragmented genome.  We map chromosome indices to maps from starting
-- position to a pair of a 'PackedLump' and its length.

newtype Genome = Genome (I.IntMap (I.IntMap (Int, PackedLump)))

emptyGenome :: Genome
emptyGenome = Genome I.empty

-- | Inserts an encoded block into a genome.  Blocks are checked for
-- unexpected overlaps and warnings are printed.  In MAF files generated
-- in an unspecified manner from EMF files, I get lots of strange
-- overlaps.  They are "fixed" by ignoring the offending blocks.  If
-- this happens, live with the damage or fix your input files.  In
-- contrast, reading the same EMF files directly, I only get warnings
-- about duplicated blocks.

insertGenome :: MonadLog m => Genome -> Int -> Int -> Int -> PackedLump -> m Genome
insertGenome (Genome m1) !c !p !l !s = Genome m1' <$ warning
  where
    !m2 = I.findWithDefault I.empty c m1
    !m2' = I.insert p (l,s) m2
    !m1' = I.insert c m2' m1

    warning = case I.splitLookup p m2 of
        (_, Just (l',s'), _)
            | l == l' && unpackLump s == unpackLump s'
                -> logMsg Notice $ EmfDuplication c p
            | otherwise
                -> logMsg Warning $ EmfCollision c p (l == l')

        (lm, _, rm) -> case (I.maxViewWithKey lm, I.minViewWithKey rm) of

            (Just ((p1, (l1, _)), _), _)
                | p1 + l1 > p
                    -> overlap p1 (p1+l1) p (p+l)

            (_, Just ((p2, (l2, _)), _))
                | p + l > p2
                    -> overlap p (p+l) p2 (p2+l2)

            _ -> pure ()

    overlap u1 v1 u2 v2 = logMsg Error $ EmfOverlap c u1 v1 u2 v2


data EmfDuplication = EmfDuplication !Int !Int deriving (Typeable, Show)
instance Exception EmfDuplication where
    displayException (EmfDuplication c p) =
        printf "duplication at %d:%d" c p

data EmfCollision = EmfCollision !Int !Int !Bool deriving (Typeable, Show)
instance Exception EmfCollision where
    displayException (EmfCollision c p ll) =
        printf "collision at %d:%d with different " c p ++ if ll then "sequences" else "blocks"

data EmfOverlap = EmfOverlap !Int !Int !Int !Int !Int deriving (Typeable, Show)
instance Exception EmfOverlap where
    displayException (EmfOverlap c u1 v1 u2 v2) =
        printf "overlap between %d:%d-%d and %d:%d-%d" c u1 v1 c u2 v2


-- | To parse MAF, we shouldn't need a reference, and in fact we don't
-- look at the reference sequence at all.  We use the reference only to
-- map chromsome names to indices.  (We allow UCSC-style names to match
-- NCBI-style names.  This happens all the time for the human genome.)
--
-- | Reads MAF.  We discard the header lines starting with '#'.  The
-- rest must be blocks.  Each block must begin with an "a" line and
-- extends to the next empty line.  The "s" lines in the blocks are
-- scanned for the two expected species, the coordinates are extracted
-- from the line for the reference sequence.  Encoded blocks are
-- accumulated in memory and finally emitted in the correct order.
-- Chromosome names are mapped to target indices according to the
-- reference genome.

parseMaf :: (MonadLog m, MonadIO m, MonadThrow m) => Maybe FilePath -> (Bytes,Bytes) -> TwoBitFile -> Genome -> ByteStream m r -> m Genome
parseMaf fp (from_spc, to_spc) chrs gnm0 = doParse gnm0 . S.lines'
  where
    doParse :: (MonadLog m, MonadIO m, MonadThrow m) => Genome -> Stream (Of B.ByteString) m r -> m Genome
    doParse gnm = inspect . Q.dropWhile (\l -> B.all isSpace l || B.head l == '#') >=> \case
        Left _                                      -> return gnm
        Right (ahdr :> lns) | "a":_ <- B.words ahdr -> do
            slns :> lns' <- Q.toList $ Q.span (not . B.all isSpace) lns
            gnm' <- foldl (>>=) (return gnm) $ do
                        ( Just from_chr, from_pos, from_len, from_rev, from_seq ) <- extract from_spc slns
                        ( _to_chr,        _to_pos,  _to_len,  _to_rev,   to_seq ) <- extract to_spc   slns
                        let (ref', smp') = B.unzip $ filter ((/=) '-' . fst) $ B.zip from_seq to_seq

                        return $ \g -> do
                            logStringLn $ printf "%d:%d-%d" (unpack $ tbc_name from_chr) from_pos (from_pos+from_len)
                            insert1 g from_rev (tbc_index from_chr) from_pos from_len ref' smp'

            doParse gnm' lns'

        Right (l :> ls) -> do
            ls' :> _ <- Q.toList $ Q.take 2 ls
            liftIO . throwIO . DataError fp $ "expected 'a' line but got\n" ++ B.unpack (B.unlines $ l : ls')


    insert1 :: (MonadLog m, MonadIO m, MonadThrow m) => Genome -> Bool -> Int -> Int -> Int -> B.ByteString -> B.ByteString -> m Genome
    insert1 g False chrm pos len ref smp = do
            plump :> _ <- encodeLumpToMem $ Q.map make_hap $ diff ref smp
            insertGenome g chrm pos len plump

    insert1 g True chrm pos len ref smp = do
            plump :> _ <- encodeLumpToMem $ Q.map make_hap $ diff (revcompl ref) (revcompl smp)
            insertGenome g chrm (pos-len+1) len plump

    extract :: Bytes -> [Bytes] -> [( Maybe TwoBitChromosome, Int, Int, Bool, Bytes )]
    extract spc lns =
        [ ( chrom, pos, len, rev, seq_ )
        | sln <- lns
        , "s":name:pos_:len_:str_:_olen:seq_:_ <- [ B.words sln ]
        , B.snoc spc '.' `B.isPrefixOf` name || spc == name
        , let chrom = findChrom (B.drop (B.length spc + 1) name) chrs
        , (pos, "") <- maybeToList $ B.readInt pos_
        , (len, "") <- maybeToList $ B.readInt len_
        , rev <- [ False | str_ == "+" ] ++ [ True | str_ == "-" ] ]


encodeGenome :: MonadIO m => TwoBitFile -> Genome -> ByteStream m ()
encodeGenome ref (Genome m1)
    | I.null m1 = liftIO . throwIO $ PebkacError "empty genome.  Is this the correct reference?"
    | otherwise = S.toByteStream $
                      encodeHeader ref <>
                      foldMap (\c -> encodeChr $ I.findWithDefault I.empty (tbc_index c) m1) (tbf_chroms ref)
  where
    -- concatenate the lumps with appropriate gaps, end with a 'Break'
    encodeChr :: I.IntMap (Int, PackedLump) -> Builder
    encodeChr m2 = I.foldrWithKey step (const $ word8 0) m2 0

    step :: Int -> (Int, PackedLump) -> (Int -> Builder) -> Int -> Builder
    step start (len, PackLump lump) k pos
        -- Could we get duplicated or overlapping blocks?  We won't deal
        -- with them properly, but we skip them just in case, so we get
        -- valid, if not correct output.
        | start >= pos = lots_of_Ns (start - pos) <> lazyByteString lump <> k (start+len)
        | otherwise    =                                                    k start

    -- Argh, this wasn't supposed to be repeated here, but it's the most
    -- straight forward way to do it.  If the encoding of 'Lump's (see
    -- 'encodeLump') ever changes, this needs to be adapted.
    lots_of_Ns :: Int -> Builder
    lots_of_Ns n
        | n == 0        = mempty
        | n < 0x3C      = word8 $ 0x40 .|. fromIntegral n
        | n < 0x100     = word8 ( 0x40 .|. 0x3F ) <>
                          word8 ( fromIntegral  n )
        | n < 0x10000   = word8 ( 0x40 .|. 0x3E ) <>
                          word8 ( fromIntegral (n             .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR`  8 .&. 0xff) )
        | n < 0x1000000 = word8 ( 0x40 .|. 0x3D ) <>
                          word8 ( fromIntegral (n             .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR`  8 .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR` 16 .&. 0xff) )
        | otherwise     = word8 ( 0x40 .|. 0x3C ) <>
                          word8 ( fromIntegral (n             .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR`  8 .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR` 16 .&. 0xff) ) <>
                          word8 ( fromIntegral (n `shiftR` 24 .&. 0xff) )


data EmfBlock = EmfBlock { emf_tree :: Tree Label
                         , emf_seqs :: [B.ByteString] }
    deriving Show

scanBlocks :: Monad m => Stream (Of B.ByteString) m r -> Stream (Of EmfBlock) m r
scanBlocks inp = do
        rngs :> inp1 <- lift . Q.toList .
                        Q.zipWith parse_seq_name (Q.enumFrom 0) .
                        Q.span (B.isPrefixOf "SEQ ") .              -- names and indices of sequences
                        Q.dropWhile (B.all isSpace) .               -- and empty lines
                        Q.dropWhile ("#" `B.isPrefixOf`) $ inp      -- get rid of comments

        lift (Q.next inp1) >>= \case
            -- We detect EOF here.  ('rngs' will be empty, too, which is not a problem.)
            Left r -> pure r
            Right (treeln, inp1a) -> do
                lift (Q.next inp1a) >>= \case
                    Right ("DATA", inp2) -> do
                        let tree = relabel rngs $ parse_tree treeln         -- topology of tree w/ indices
                        datalns :> remainder <- lift $ Q.toList $ Q.map (B.map toUpper) $ Q.span ("//" /=) inp2
                        EmfBlock tree (B.transpose datalns) `Q.cons` scanBlocks (Q.drop 1 remainder)
                    _ -> throw $ DataError Nothing "expected DATA"
  where
    parse_seq_name i s = Label i (spc, B.drop 1 race) (st $ Range (Pos sqn (beg-1)) (end-beg+1))
      where
        ("SEQ":sr:sqn:beg':end':st':_) = B.words s
        (spc,race) = B.break ('_' ==) sr
        beg = maybe 0 fst $ B.readInt beg'
        end = maybe 0 fst $ B.readInt end'
        st  = if st' == "1" then id else reverseRange

    parse_tree s = case P.parse (P.string "TREE " *> treep <* P.char (c2w ';')) "" s of
        Left  e -> throw $ DataError Nothing $ "parse of TREE line failed with " ++ show e ++ " on " ++ show s
        Right r -> r

    treep :: P.Parsec () Bytes (Tree (Bytes, Double))
    treep =   Branch <$ P.char (c2w  '(') <*> treep <* P.char (c2w  ',') <*> treep <* P.char (c2w  ')') <*> label
            <|> Leaf <$> label

    label :: P.Parsec () Bytes (Bytes, Double)
    label = do x <- P.takeWhileP Nothing $ \c -> c2w '0' <= c && c <= c2w '9' ||
                                                 c2w 'A' <= c && c <= c2w 'Z' ||
                                                 c2w 'a' <= c && c <= c2w 'z' ||
                                                 c == c2w '_' || c == c2w '.'
               y <- P.char (c2w '[') *> P.oneOf (map c2w "+-")
               z <- P.char (c2w ']') *> P.char (c2w  ':') *> P.float
               pure (x `B.snoc` '[' `B.snoc` w2c y `B.snoc` ']', z)

data Label = Label Int Species Range deriving Show
data Tree label = Branch (Tree label) (Tree label) label | Leaf label deriving Show

relabel :: [Label] -> Tree (B.ByteString, Double) -> Tree Label
relabel ls (Leaf       (lbl,_)) = Leaf                                 (find_label lbl ls)
relabel ls (Branch u v (lbl,_)) = Branch (relabel ls u) (relabel ls v) (find_label lbl ls)

find_label :: B.ByteString -> [Label] -> Label
find_label lbl ls = case filter ((==) lbl . get_short) ls of
    [l] -> l
    []  -> throw $ DataError Nothing $ "not found: " ++ show lbl ++ " in " ++ show ls
    _   -> throw $ DataError Nothing $ "ambiguous: " ++ show lbl ++ " in " ++ show ls

get_short :: Label -> B.ByteString
get_short (Label _ (spc,race) (Range (Pos chrom pos) len)) = B.concat $
    [ B.map toUpper (B.take 1 spc),
      B.take 3 race, B.singleton '_',
      chrom, B.singleton '_' ] ++
    if pos >= 0 then [ B.pack (show $ 1+pos), B.singleton '_',
                       B.pack (show $ pos+len), "[+]" ]
                else [ B.pack (show $ 1-pos-len), B.singleton '_',
                       B.pack (show $ -pos), "[-]" ]

type Species = (B.ByteString,B.ByteString)
type Selector = Species -> Tree Label -> [(Label, [Label])]

-- | Find closely related sequence in another species.  We look for a
-- tree that has homo on one side, but no chimp, and at least one chimp
-- on the other.  For each find of home, we generate one result, but all
-- chimps in the other branch are collected.
species_to_species :: Species -> Species -> Tree Label -> [(Label, [Label])]
species_to_species homo pan = subtrees
  where
    subtrees (Leaf       _) = []
    subtrees (Branch u v _) = case ( tree_has homo u, tree_has pan u, tree_has homo v, tree_has pan v ) of
        (   hs,    [   ],    _, cs@(_:_) ) -> [ (h,cs) | h <- hs ] ++ subtrees v
        (    _, cs@(_:_),   hs,    [   ] ) -> [ (h,cs) | h <- hs ] ++ subtrees u
        (    _,        _,    _,        _ ) -> subtrees u ++ subtrees v

-- Find common ancestor.  The common ancestor (with, say, chimp) is the
-- root of a tree that has homo on one side but no chimp, and chimp on
-- the other.  The ancestral sequence may well apply to more than one
-- coordinate on homo, and we may find more than one suitable tree.
--
-- We return two labels, the first is the human sequence, the second the
-- ancestor.  Multiple hits to different subtrees are possible.

ancestor_to_species :: Species -> Species -> Tree Label -> [(Label, [Label])]
ancestor_to_species homo pan = subtrees
  where
    subtrees (Leaf          _) = []
    subtrees (Branch u v albl) = case ( tree_has homo u, tree_has pan u, tree_has homo v, tree_has pan v ) of
        (   hs,  [],    _, _:_ ) -> [ (h,[albl]) | h <- hs ] ++ subtrees v
        (    _, _:_,   hs,  [] ) -> [ (h,[albl]) | h <- hs ] ++ subtrees u
        (    _,   _,    _,   _ ) -> subtrees u ++ subtrees v

tree_has :: Species -> Tree Label -> [Label]
tree_has s (Leaf lbl@(Label _ spc _)) = [ lbl | s == spc ]
tree_has s (Branch           u' v' _) = tree_has s u' ++ tree_has s v'

data EmfLengthMismatch = EmfLengthMismatch Bytes Int [Int] deriving (Typeable, Show)
instance Exception EmfLengthMismatch where
    displayException (EmfLengthMismatch c rl ls) =
        printf "declared length %d of %s does not match actual length %s" rl (unpack c) (show ls)

-- | Returns a 'Fragment' for a subset of the species (say, the Chimps)
-- in a tree relative to a specific one (usually the Human).  If we find
-- multiple Human sequences, we return mutiple results.  If we find
-- multiple Chimps, we turn them into a consensus sequence and return
-- the result for the consensus sequence.
collectTrees :: (MonadIO m, MonadLog m, MonadThrow m) => TwoBitFile -> ( Tree Label -> [(Label, [Label])] ) -> Genome -> EmfBlock -> m Genome
collectTrees chrs select genome block
    = foldlM (\g f -> f g) genome

            [ \g -> do plump :> (lns :> _) <- encodeLumpToMem . Q.toList . lengthLumps . Q.copy . Q.map make_hap
                                                    $ diff ref_seq smp_seq
                       g' <- insertGenome g tbc_index (p_start (r_pos rref)) (r_length rref) plump
                       logStringLn $ printf
                            "Inserted %s:%d-%d, %d bytes."
                            (unpack tbc_name)
                            (p_start (r_pos rref))
                            (p_start (r_pos rref) + r_length rref)
                            (L.length (unpackLump plump))

                       unless (lns == [r_length rref]) $ logMsg Error $
                            EmfLengthMismatch tbc_name (r_length rref) lns
                       return $! g'
            | (Label iref _ rref_, tgt_lbls) <- select $ emf_tree block
            , TBC{..} <- maybeToList $ findChrom (p_seq (r_pos rref_)) chrs
            , let ref_seq_ = emf_seqs block !! iref
            , let smp_seq_ = consensus_seq [ emf_seqs block !! itgt | Label itgt _ _ <- tgt_lbls ]
            , let (rref, ref_seq, smp_seq) =
                    if p_start (r_pos rref_) >= 0
                    then (             rref_,          ref_seq_,          smp_seq_)
                    else (reverseRange rref_, revcompl ref_seq_, revcompl smp_seq_) ]

revcompl :: B.ByteString -> B.ByteString
revcompl = B.reverse . B.map compl
  where
    compl 'A' = 'T' ; compl 'T' = 'A' ; compl 'a' = 't' ; compl 't' = 'a'
    compl 'C' = 'G' ; compl 'G' = 'C' ; compl 'c' = 'g' ; compl 'g' = 'c'
    compl  x  =  x


consensus_seq :: [B.ByteString] -> B.ByteString
consensus_seq [    ] = B.empty
consensus_seq [  x ] = x
consensus_seq (x:xs)
    | all (== B.length x) (map B.length xs) =
        B.pack [ foldl' (\r c -> if r == c then r else 'N')
                        (B.index x i) (map (`B.index` i) xs)
               | i <- [0 .. B.length x -1] ]
    | otherwise =
        throw $ DataError Nothing "consensus_seq: sequences have unequal lengths."


emf_options :: Parser (LIO ())
emf_options = emf_main
    <$> many (strArgument (metavar "EMF-FILE"))
    <*> strOption (short 'o' <> long "output"         <> metavar "FILE" <> help "Write output to FILE (.hef)")
    <*> strOption (short 'r' <> long "reference"      <> metavar "FILE" <> help "Read reference from FILE (.2bit)")
    <*> option w2 (short 'R' <> long "ref-species"    <> metavar "NAME" <> help "Set reference species to NAME"
                             <> value ("homo","sapiens"))
    <*> (flip species_to_species <$> option w2
                  (short 'S' <> long "sample-species" <> metavar "NAME" <> help "Import species NAME") <|>
         flip ancestor_to_species <$> option w2 (value ("pan","troglodytes") <>
                   short 'A' <> long "ancestor-of"    <> metavar "NAME" <> help "Import ancestor with species NAME"))
  where
    w2 = maybeReader $ \s -> Just $ case B.split ',' (B.pack (map toLower s)) of
                a:b:_ -> (a,b) ; a:_ -> (a,B.empty) ; _ -> (B.empty,B.empty)

    emf_main :: [FilePath] -> FilePath -> FilePath -> Species -> Selector -> LIO ()
    emf_main emfs emf_output emf_reference emf_ref_species emf_select = do
        ref <- liftIO $ openTwoBit emf_reference

        let cons = collectTrees ref (emf_select emf_ref_species)
        let getdir dir = map (dir </>) . filter (".emf.gz" `isSuffixOf`) <$> liftIO (getDirectoryContents dir)

        !genome <- do fs <- case emfs of
                                -- this is just so the one path at MPI that makes sense
                                -- doesn't need to be typed over and over again
                                [ ] -> liftIO $ getdir "/mnt/expressions/martin/sequence_db/epo/epo_6_primate_v66"
                                [f] -> liftIO $ doesDirectoryExist f >>= bool (return emfs) (getdir f)
                                _   -> return emfs

                      foldM (\g (i,fp) -> do
                                logStringLn $ printf "File %d of %d: %s" (i::Int) (length fs) fp
                                handle (\(DataError _ m) -> liftIO . throwIO $ DataError (Just fp) m) $
                                    streamInput fp $
                                        Q.foldM_ cons (return g) return . scanBlocks .
                                        S.lines' . S.gunzip
                            ) emptyGenome $ zip [1..] fs

        S.writeFile emf_output . S.gzip $ encodeGenome ref genome

