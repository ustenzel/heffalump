module ShameMS
        ( blockToShame, bsfs_options, shame_options
        , physicalBlocks, MaxPhysical(..)
        , observedBlocks, NumObserved(..)
        ) where

import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude
import Bio.Streaming                    ( Stream, yields, mapsM_, mapped )
import Bio.Streaming.Vector             ( concatVectors )
import Options.Applicative

import qualified Bio.Streaming.Prelude           as Q
import qualified Data.ByteString.Builder         as B
import qualified Data.Map.Strict                 as M
import qualified Data.Vector                     as V
import qualified Data.Vector.Storable            as U
import qualified Data.Vector.Storable.Mutable    as UM

import Bed     ( readBed, filterWithBed, filterRefWithBed )
import Treemix ( InputConf(..), input_options, VarDensity(..), density_option )

common_options :: Parser (InputConf, Maybe (Int, Maybe Int))
common_options =
    input_options <.>
    optional (option auto (short 'l' <> long "block-length" <>
                           metavar "LEN" <> help "Blocks of maximum length LEN") <.>
              optional (option auto (short 'm' <> long "num-observed" <>
                                     metavar "NUM" <> help "Number of observed sites per block")))
  where (<.>) = liftA2 (,)

bsfs_options :: Parser (LIO ())
bsfs_options = uncurry bsfs_main <$> common_options
  where
    bsfs_main InputConf{..} conf_blocks = liftIO $ do
        decodeManyRef input_reference input_hefs $ \ref inps -> do
            region_filter <- liftIO $ readBed input_regions $ tbf_chrnames ref

            bsfs :> () <- Q.fold (\acc v -> M.insertWith (+) v (1::Int) acc) M.empty id $
                          mapped to_bsfs $
                          do_blocks conf_blocks $
                          bool id (Q.map make_simple) input_nosplit $
                          addRef (MaxHole 49) (maybe id filterRefWithBed region_filter ref) $
                          mergeLumps (NumOutgroups input_noutgroups) inps
            M.foldrWithKey (\k v z -> putStrLn (shows k " => " ++ show v) >> z) (return ()) bsfs

    make_simple Variants{..}
        | V.length c_vars /= 1
            = let cs = U.map (flip AC 0 . ac_num_ref) $ snd $ V.head c_vars
              in Variants { c_vars = V.singleton (V2b 255, cs), .. }
    make_simple v = v

    to_bsfs :: PrimMonad m => Stream (Of Clump) m r -> m (Of (U.Vector Int) r)
    to_bsfs = Q.foldM step (UM.new 0) U.unsafeFreeze
      where
        step v Variants{..} | allObserved c_vars       = V.foldM step1 v c_vars
        step v Clump   {..} | U.all (/= 0) c_refcounts = do v' <- resize (U.length c_refcounts) v
                                                            bump v' (UM.length v' - 1) c_len
                                                            return v'
        step v _ = return v

        step1 v (_, counts) = do v' <- resize (U.length counts) v
                                 bump v' (to_index counts) 1
                                 return v'

        bump v i x = UM.read v i >>= UM.write v i . (+) x

        resize l v = if UM.length v >= l' then return v else UM.grow v (l' - UM.length v)
            where
                l' = div (3^l + 1) 2

        to_index :: U.Vector AlleleCounts -> Int
        to_index = abs . U.foldl' (\acc cs -> 3 * acc + to_index1 cs) 0

        -- We already discarded sites where any sample has an N.  Now Ns
        -- count as reference---we're counting mutations, after all.
        to_index1 (AC _ 0) =  1
        to_index1 (AC 0 _) = -1
        to_index1 (AC _ _) =  0

shame_options :: Parser (LIO ())
shame_options = uncurry shame_main <$> common_options <*> density_option
  where
    shame_main InputConf{..} conf_blocks conf_density = liftIO $ do
        decodeManyRef input_reference input_hefs $ \refs inps -> do
            region_filter <- readBed input_regions $ tbf_chrnames refs

            mapsM_ ( maybe id (\(l,_) -> (hPutStrLn stdout ("\n//\nblockSize_" ++ show l) >>)) conf_blocks
                   . blockToShame stdout
                   . maybe id filterWithBed region_filter
                   . case (conf_density, input_nosplit) of
                                (Sparse,  True) -> expandVars
                                (Sparse, False) -> expandSimpleVars
                                (Dense,   True) -> expandVarsDense
                                (Dense,  False) -> expandSimpleVarsDense ) $
                do_blocks conf_blocks $
                addRef (MaxHole 49) refs $
                mergeLumps (NumOutgroups $ case conf_density of Dense -> 0 ; Sparse -> input_noutgroups) inps


do_blocks :: Monad m => Maybe (Int, Maybe Int) -> Stream (Of Clump) m r -> Stream (Stream (Of Clump) m) m r
do_blocks            Nothing   = yields
do_blocks (Just  (l, Nothing)) = physicalBlocks (MaxPhysical l)
do_blocks (Just  (l, Just  m)) = observedBlocks (NumObserved m) (MaxPhysical l)

blockToShame :: Handle -> Stream (Of Variant) IO r -> IO r
blockToShame h = Q.next >=> oneBlock
  where
    oneBlock (Left       r) = return r
    oneBlock (Right (v1,s)) = do
        let stride = U.length (v_calls v1)
        ff :> r <- Q._first (fmap (Block stride) . U.unsafeFreeze) =<< concatVectors (Q.map smashVariants (Q.cons v1 s))
        unless (nullBlock ff) $
            B.hPutBuilder h $ foldMap
                (\ind -> oneLine fstW ind <> oneLine sndW ind)
                (individuals ff)
        return r

    oneLine :: (TwoNucs -> Nuc2b) -> U.Vector TwoNucs -> B.Builder
    oneLine which = U.foldr ((<>) . B.char7 . toRefCode . which) (B.char7 '\n')

    smashVariants :: Variant -> U.Vector TwoNucs
    smashVariants Variant{..} =
        U.map (smashVariant v_ref v_alt) v_calls

    smashVariant :: Nuc2b -> Var2b -> AlleleCounts -> TwoNucs
    smashVariant r a = \case
        AC 0 0 -> TwoNucs n n
        AC _ 0 -> TwoNucs r r
        AC 0 _ -> TwoNucs v v
        AC _ _ -> TwoNucs r v
      where
        n = N2b 15
        v = appVar r a


-- | A pair of nucleotides with compact storage.
data TwoNucs = TwoNucs { fstW :: !Nuc2b, sndW :: !Nuc2b }

instance Storable TwoNucs where
    sizeOf _ = 1
    alignment _ = 1
    poke p (TwoNucs (N2b a) (N2b b)) = poke (castPtr p) (a .|. shiftL b 4)
    peek p = (\x -> TwoNucs (N2b $ x .&. 0xF) (N2b $ shiftR x 4 .&. 0xF)) <$> peek (castPtr p)

-- | Our variant data comes in as a stream of variants, but for output
-- we need a stream of individuals.  Put another way, the rows of our
-- data matrix are variants, and we need to transpose it so the variants
-- are in the columns.  This means we need to buffer at least a block in
-- memory.
--
-- To keep storage compact, we encode two alleles into a single 'Word8',
-- then store the whole matrix in a 'U.Vector' 'TwoNucs'.  Use 'variants'
-- to access the 'Block' as a list of variants (traverse row-by-row) or
-- 'individuals' to access it as a list of individuals (traverse
-- column-by-column).

data Block = Block !Int                 -- ^ the stride (number of individuals)
                   !(U.Vector TwoNucs)  -- ^ the data matrix

nullBlock :: Block -> Bool
nullBlock (Block _ v) = U.null v

_variants :: Block -> V.Vector (U.Vector TwoNucs)
_variants (Block w ff) =
    V.map (\i -> U.slice i w ff) $
    V.enumFromStepN 0 w h
  where
    h = U.length ff `div` w

individuals :: Block -> V.Vector (U.Vector TwoNucs)
individuals (Block w ff) =
    V.map (\j -> U.map (ff U.!) $ U.enumFromStepN j w h) $
    V.enumFromN 0 w
  where
    h = U.length ff `div` w

