-- | Code to read a BAM file in.  We pileup, then sample a base
-- randomly.  We end with the same format we would get if we ran 'vcflc'
-- on an appropriately generated VCF file.

module Bamin ( bam_options ) where

import Bio.Bam               hiding ( Ns )
import Bio.Bam.Pileup
import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Bio.Prelude           hiding ( Ns )
import Options.Applicative
import System.Random                ( StdGen, newStdGen, randomR )

import qualified Bio.Streaming.Bytes            as S
import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString.Lazy           as L
import qualified Data.IntMap.Strict             as I
import qualified Data.Vector                    as V
import qualified Data.Vector.Unboxed            as U

import Util

data Pick = Ostrich | Ignore_T | Ignore_A | Ignore_C | Ignore_G
          | Stranded | UDG | Kay | Mateja | Mateja2
  deriving Show

bam_options :: Parser (LIO ())
bam_options = bam_main
    <$> many (strArgument (metavar "BAM-FILE"))
    <*> strOption   (short 'o' <> long "output"    <> metavar "FILE" <> help "Write output to FILE (.hef)")
    <*> strOption   (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)")
    <*> option qual (short 'q' <> long "min-qual"  <> metavar "QUAL" <> help "Discard bases below quality QUAL")
    <*> option qual (short 'm' <> long "min-mapq"  <> metavar "QUAL" <> help "Discard reads below mapq QUAL")
    <*> switch                   (long "deaminate"                   <> help "Artificially deaminate")
    <*> (flag'  Nothing        (long "call"     <> help "Call and use genotypes") <|>
         flag' (Just Ostrich)  (long "pick"     <> help "Blindly pick a read at each site") <|>
         flag' (Just Ignore_T) (long "ignore-t" <> help "Ignore T on forward strand") <|>
         flag' (Just Ignore_A) (long "ignore-a" <> help "Ignore A on forward strand") <|>
         flag' (Just Ignore_C) (long "ignore-c" <> help "Ignore C on forward strand") <|>
         flag' (Just Ignore_G) (long "ignore-g" <> help "Ignore G on forward strand") <|>
         flag' (Just Stranded) (long "stranded" <> help "Call only G,A on forward strand") <|>
         flag' (Just UDG)      (long "udg"      <> help "Simulate UDG treatment") <|>
         flag' (Just Kay)      (long "kay"      <> help "Ts become Ns") <|>
         flag' (Just Mateja)   (long "mateja"   <> help "Weird stuff") <|>
         flag' (Just Mateja2)  (long "mateja2"  <> help "Weird stuff") <|>
         pure (Just Ostrich))
    <*> option auto (long "snip" <> metavar "NUM" <> help "Ignore the first NUM bases in each read")
    <*> option auto (long "snap" <> metavar "NUM" <> help "Ignore the last NUM bases in each read")
  where
    qual :: ReadM Qual
    qual = fmap Q auto

    bam_main :: [FilePath] -> FilePath -> FilePath -> Qual -> Qual -> Bool -> Maybe Pick -> Int -> Int -> LIO ()
    bam_main bams conf_bam_output conf_bam_reference conf_min_qual
             conf_min_mapq conf_deaminate conf_pick conf_snip conf_snap =
      do
        ref      <- liftIO $ openTwoBit conf_bam_reference
        deam_gen <- liftIO $ newStdGen
        samp_gen <- liftIO $ newStdGen

        withOutputFile conf_bam_output                                    $ \hdl ->
            mergeInputsOn coordinates bams                                $ \hdr ->
                S.hPut hdl
                . S.gzip
                . encodePiles ref (meta_refs hdr)
                . maybe (Q.map call_pile) (sample_piles samp_gen) conf_pick
                . withRef ref
                . bool (Q.map (fmap eightToTen)) (deaminate_stream deam_gen) conf_deaminate
                . Q.map collate_pile
                . pileup
                . Q.concat
                . Q.map (dissect (DmgToken 0))
                . Q.filter ((\b -> b_mapq b >= conf_min_mapq && (not (isAlternative b) || isPaired b)) . unpackBam)
                . progressPos coordinates "bamin" (meta_refs hdr) 10000
      where
        collate_pile p =
            Var { v_refseq = p_refseq p
                , v_loc    = p_pos p
                , v_call   = count8fromVec $ U.accum (+) (U.replicate 8 0)
                    [ (fromIntegral (unN (db_call b)) +o, 1)
                    | (o,b) <- map ((,) 0) (fst $ p_snp_pile p) ++ map ((,) 4) (snd $ p_snp_pile p)
                    , db_qual b >= conf_min_qual
                    , db_dmg_pos b >= conf_snip || db_dmg_pos b < negate conf_snap ] }

        count8fromVec v = Count8 (v U.! 0) (v U.! 1) (v U.! 2) (v U.! 3)
                                 (v U.! 4) (v U.! 5) (v U.! 6) (v U.! 7)

-- Our varcall: position, base, and either the counters for the random
-- sampling or the variant code
data Var a = Var { v_refseq  :: !Refseq
                 , v_loc     :: !Int
                 , v_call    :: a }
    deriving (Show, Functor)

-- | Bases counted on raw data:  TCAG on forward aligned reads, TCAG on
-- reverse aligned reads
data Count8 = Count8 !Int !Int !Int !Int !Int !Int !Int !Int

-- | Bases counted after stupid filtering: TCAGU on forward aligned
-- reads, TCAGU on reverse aligned reads
data Count10 = Count10 !Int !Int !Int !Int !Int !Int !Int !Int !Int !Int

-- | Bases counted after final selection: TCAGU on any read
data Count5 = Count5 !Int !Int !Int !Int !Int

eightToTen :: Count8 -> Count10
eightToTen (Count8 t c a g t' c' a' g') = Count10 t c a g 0 t' c' a' g' 0


withRef :: MonadThrow m => TwoBitFile -> Stream (Of (Var a)) m () -> Stream (Of (Pair Nuc2b (Var a))) m ()
withRef cs0 = nextChrom (map (`tbc_fwd_seq` 0) $ toList $ tbf_chroms cs0) (Refseq 0)
  where
    nextChrom [    ]  _ = const $ pure ()
    nextChrom (c:cs) rs = generic cs c rs 0

    generic cs c !rs !pos = lift . inspect >=> \case
        Left _  -> pure ()
        Right (var1 :> s)
            | rs /= v_refseq var1
                -> nextChrom cs (succ rs) (wrap (var1 :> s))
            | v_loc var1 < pos
                -> lift . throwM $ DataError Nothing "unsorted bam?"
            | Just (rb,c') <- unconsRS $ dropRS (v_loc var1 - pos) c
                -> wrap ((:!:) rb var1 :> generic cs c' rs (v_loc var1 + 1) s)
            | otherwise
                -> generic cs RefEnd rs (v_loc var1 + 1) s


sample_piles :: Monad m => StdGen -> Pick -> Stream (Of (Pair Nuc2b (Var Count10))) m r -> Stream (Of (Var Lump)) m r
sample_piles g0 pick = go g0
  where
    go g = lift . inspect >=> \case
        Left  r -> pure r
        Right ((N2b rb :!: var1) :> s)
                -> let (N2b nc, g') = sample_from (post_collect (N2b rb) pick (v_call var1)) g
                   in wrap (var1 { v_call = enc_var (xor rb nc) } :> go g' s)

    enc_var :: Word8 -> Lump
    enc_var 0 = Eqs1 1     -- ref equals alt, could both be N
    enc_var 1 = Trans1     -- transition
    enc_var 2 = Compl1     -- complement
    enc_var 3 = TCompl1    -- trans-complement
    enc_var _ = Ns 1       -- ref is N, or alt is N


-- This uses a quality of 23 throughout, calls a genotype if it has a
-- quality better than 30, assumes homozygous variants at rate 1/3000
-- and heterozygous variants at rate 2/3000.  All these constants could
-- be configured, if anyone cared.  XXX
--
-- Note that this produces barely useable junk (lots of Ns) for low
-- coverage samples.  Just don't do it.

call_pile :: Pair Nuc2b (Var Count10) -> Var Lump
call_pile (N2b rb :!: var@Var{ v_call = Count10 t c a g u t' c' a' g' u' })
    | rb >=  3  = var { v_call = Ns 1 }             -- ref is N
    | q2 >= 30  = var { v_call = code2 V.! i2 }
    | q1 >= 30  = var { v_call = code1 V.! i1 }
    | otherwise = var { v_call = Ns 1 }
  where
    code1 = V.fromListN  4 [ Eqs1 1, Trans1, Compl1, TCompl1 ]
    code2 = V.fromListN 10 [ Eqs2 1, Trans2, Compl2, TCompl2
                           , RefTrans, RefCompl, RefTCompl
                           , TransCompl, ComplTCompl, TransTCompl ]

    counts :: U.Vector Int
    counts = U.replicate 4 0 U.//
                [ ( fromIntegral $ xor rb 0,  u + t + t')
                , ( fromIntegral $ xor rb 1,      c + c')
                , ( fromIntegral $ xor rb 2, u' + a + a')
                , ( fromIntegral $ xor rb 3,      g + g') ]

    -- Simple approximation:  neg-log-likelihood (base 10) for a correct
    -- base is 0, for an incorrect base 23.  Prior for homozygous
    -- variant is 36 (about 1/3000).
    lHom = U.generate 4 $ \i -> 23 * (U.sum counts - counts U.! i) + if i == 0 then 0 else 36

    -- Prior for heterozygous variant is 33 (about 2/3000).
    lHet = U.fromListN 6 $
            [ 23 * (counts U.! i + counts U.! j) + 3 * U.sum counts + 33
            | (i,j) <- [(2,3),(1,3),(1,2),(0,3),(0,2),(0,1)] ]

    (i1,q1) = minAndQ lHom
    (i2,q2) = minAndQ (lHom U.++ lHet)

    minAndQ v = go 0 0 maxBound maxBound
      where
        go !i !j !m !n
            | i == U.length v = (j,n-m)
            | v U.! i < m = go (i+1) i (v U.! i) m
            | v U.! i < n = go (i+1) j m (v U.! i)
            | otherwise   = go (i+1) j m n


encodePiles :: TwoBitFile -> Refs -> Stream (Of (Var Lump)) LIO r -> ByteStream LIO r
encodePiles ref tgts stream = do
    map1 :> r <- lift . Q.fold (\m -> maybe m (\(k,v) -> I.insert k v m)) I.empty id .
                 mapped scan1 .  Q.groupBy ((==) `on` v_refseq) $ stream

    when (I.null map1) . liftIO . throwM $
        DataError Nothing "Found only unexpected sequences.  Is this the right reference?"

    S.toByteStream $ encodeHeader ref
    forM_ (tbf_chroms ref) $ \c ->
            S.fromLazy $ unpackLump $ I.findWithDefault noLump (tbc_index c) map1
    return r
  where
    scan1 :: Stream (Of (Var Lump)) LIO r -> LIO (Of (Maybe (Int, PackedLump)) r)
    scan1 = inspect >=> either (pure . (:>) Nothing) (\(v :> s) -> scan2 (v_refseq v) (Q.cons v s))

    scan2 :: Refseq -> Stream (Of (Var Lump)) LIO r -> LIO (Of (Maybe (Int, PackedLump)) r)
    scan2 rs s =
        let rn = sq_name $ getRef tgts rs
        in case findChrom rn ref of

            Nothing -> do logStringLn $ printf "Skipping %s." (unpack rn)
                          (:>) Nothing <$> Q.effects s

            Just  i -> do logStringLn $ printf "Target %s becomes index %d." (unpack rn) (tbc_index i)
                          lump :> r <- encodeLumpToMem $ importPile s
                          logStringLn $ printf "Target %s became index %d, %d bytes."
                                               (unpack rn) (tbc_index i) (L.length $ unpackLump lump)
                          return $ Just (tbc_index i, lump) :> r


importPile :: MonadThrow m => Stream (Of (Var Lump)) m r -> Stream (Of Lump) m r
importPile = normalizeLump . go 0
  where
    go !pos = lift . inspect >=> \case
        Left r -> Q.cons Break (pure r)
        Right (var1 :> s)
            | v_loc var1 >= pos -> do
                when (v_loc var1 > pos) . Q.yield . Ns $ v_loc var1 - pos   -- gap, creates Ns
                Q.yield $ v_call var1                                       -- variant
                go (v_loc var1 + 1) s

            | otherwise -> throw . DataError Nothing $
                                "Got variant position " ++ show (v_refseq var1, v_loc var1) ++
                                " when expecting " ++ show pos ++ " or higher."


deaminate_stream :: Monad m => StdGen -> Stream (Of (Var Count8)) m r -> Stream (Of (Var Count10)) m r
deaminate_stream = go
  where
    go g = lift . inspect >=> \case
        Left      r     -> pure r
        Right (v :> s') -> let (w, g') = deaminate (v_call v) g
                           in wrap (v { v_call = w } :> go g' s')

-- | Let's say we count bases A,C,G,T (not N, it's unneeded), separately
-- for the strands.  So we get 8 numbers as input.  We append another
-- two, which will become uracil(!) counts.
deaminate :: Count8 -> StdGen -> (Count10, StdGen)
deaminate (Count8 t c a g t' c' a' g') gen0 = (Count10  t (c-dc) a g dc  t' c' a' (g'-dg) dg, gen2)
  where
    (dc, gen1) = ifrac 50 c  gen0
    (dg, gen2) = ifrac 50 g' gen1

    -- Integer fraction.  ifrac x f returns either floor(x/f) or
    -- ceil (x/f) such that E(ifrac x f) == x/f.
    ifrac f x g0 = let (p,g1) = randomR (0,f-1) g0
                       (y, r) = x `divMod` f
                   in ( y + fromEnum (p `mod` f < r), g1 )



-- | We receive modified (i.e. deaminated) bases here and only vary
-- the way we count.  No randomness involved here.
post_collect :: Nuc2b -> Pick -> Count10 -> Count5
post_collect ref pp (Count10 t c a g u t' c' a' g' u') = go pp
  where
    -- Most of the time, u acts like t and u' like a'.
    -- Ostrich selection method:  pick blindly
    go Ostrich = Count5 (t+t'+u) (c+c') (a+a'+u') (g+g') 0

    -- "Deal" with deamination by ignoring possibly broken bases:
    -- T on forward, A on backward strand.
    go Ignore_T = Count5 t' (c+c') a (g+g') 0

    -- Analogous to Ignore_T, in case someone wants a control experiment...
    go Ignore_A = Count5 (t+u) (c+c') (a'+u') (g+g') 0
    go Ignore_C = Count5 (t+t'+u) c' (a+a'+u') g  0
    go Ignore_G = Count5 (t+t'+u) c  (a+a'+u') g' 0

    -- Call only G and A on the forward strand, C and T on the reverse
    -- strand.  This doesn't need to be combined with simulation code:
    -- Whenever a C could turn into a T, we ignore it either way.
    go Stranded = Count5 t' c' a g 0

    -- Simulated UDG treatment: Uracils vanish without trace.
    go UDG = Count5 (t+t') (c+c') (a+a') (g+g') 0

    -- If we pick a T, we turn it into an N.
    go Kay = Count5 t' (c+c') a (g+g') (t+a'+u'+u)

    -- Mateja's method depends on the reference allele:  if the
    -- reference is C and we see at least one (forward) T, we sample
    -- from reverse-strand reads only.  Same for G/A.
    go Mateja | isC &&  t+u  > 0 = Count5    t'       c'   (a'+u')     g'  0
              | isG && a'+u' > 0 = Count5  (t+u)      c      a         g   0
              | otherwise        = Count5 (t+t'+u) (c+c') (a+a'+u') (g+g') 0

    -- Mateja's second method depends on the reference allele:  if the
    -- reference is C, we ignore the forward Ts and sample from the rest.
    go Mateja2 | isC       = Count5    t'    (c+c') (a+a'+u') (g+g') 0
               | isG       = Count5 (t+t'+u) (c+c')    a      (g+g') 0
               | otherwise = Count5 (t+t'+u) (c+c') (a+a'+u') (g+g') 0

    isC = ref == N2b 1
    isG = ref == N2b 3


-- | Takes a random sample from prepared counts.
sample_from :: Count5 -> StdGen -> (Nuc2b, StdGen)
sample_from (Count5 t c a g n) gen = do
    let s = t + c + a + g + n
    if s == 0
        then (nucN, gen)
        else let (i, gen') = randomR (0,s-1) gen
                 nn | i < t             = nucT
                    | i < t + c         = nucC
                    | i < t + c + a     = nucA
                    | i < t + c + a + g = nucG
                    | otherwise         = nucN
             in (nn, gen')

