-- | Support for legacy files.  Transparent support is awkward, so we
-- only implement a way to upgrade them.  (If you have legacy
-- heffalumps, of all things, this limited support isn't your biggest
-- trouble.)

module Bio.Heffa.Legacy ( legacy_options ) where

import Bio.Heffa.Genome
import Bio.Prelude               hiding ( Ns )
import Bio.Streaming                    ( Stream, streamInput )
import Bio.Streaming.Bytes              ( ByteStream, uncons )
import Bio.Streaming.Prelude            ( cons, next, effects )
import Control.Monad.Log                ( PanicCall(..) )
import Options.Applicative
import System.Directory                 ( doesDirectoryExist, getDirectoryContents, renameFile )

import qualified Bio.Heffa.Lump             as L
import qualified Bio.Streaming.Bytes        as S
import qualified Bio.Streaming.Prelude      as Q
import qualified Data.ByteString            as B
import qualified Data.Vector.Unboxed        as U


-- ^ A genome is encoded by taking the difference to the reference and
-- run-length coding the result.  All stretch lengths are even, so we
-- pre-divide them by two.
--
-- This encoding had a number of unfixable problems, so it is only
-- supported for backwards compatibility:  the encoding in pairs created
-- more problems than it solved, it is impossible to encode indels,
-- haploid and diploid calls cannot be mixed.

data Stretch = Ns   !Int64
             | Eqs  !Int64
             | Eqs1 !Int64
             | Chrs !Word8 !Word8
             | Break

-- We need to store 11 different symbols: "NACGTMRWSYK"  Long
-- matching stretches and long stretches of no call ('N') shall be
-- run-length encoded.  Since 11^2 == 121 < 128, we pack two symbols and
-- a signal bit into one byte.  Also, to avoid messing with nybbles, all
-- counts shall be multiples of two.
--
-- 0xxxxxxxb: two symbols are encoded as x+11*y where x and y are the
--            positions of the symbols in the string above.
-- 10000000b: 'Break'
-- 100xxxxxb: 1-31 Ns
-- 1010xxxxb: together with the next byte, 0-8191 Ns
-- 10110xxxb: together with the next two bytes, 0-1048575 Ns
-- 101110xxb: together with the next three bytes, 0-134217727 Ns
-- 1011110xb: together with the next four bytes, 0-17179869183 Ns
--            (this shouldn't be needed)
-- 110xxxxxb: 0-31 matches
-- 1110xxxxb: together with the next byte, 0-8191 matches
-- 11110xxxb: together with the next two bytes, 0-1048575 matches
--            (this will rarely be needed)
-- 111110xxb: together with the next three bytes, 0-134217727 matches
--            (this shouldn't be needed)
-- 1111110xb: together with the next four bytes, 0-17179869183 matches
--            (this shouldn't be needed)

-- Decoding produces one stretch until it finds the end marker or runs
-- out of input.  We shall get one 'Stretch' for each chromosome.

decode_dip, decode_hap :: MonadThrow m => Maybe FilePath -> ByteStream m r -> Stream (Of Stretch) m r
decode_dip = decode_v0 id Eqs
decode_hap = decode_v0 nc Eqs1
  where
    nc x |    x == 0 = 0
         |    x <= 4 = x+10
         | otherwise = x


-- | Decode for original format, parameterized so it can be used for
-- haploid and diploid individuals (aka "high coverage" and "low
-- coverage" input).
{-# DEPRECATED decode_v0 "Switch to Lumps" #-}
decode_v0 :: MonadThrow m => (Word8 -> Word8) -> (Int64 -> Stretch) -> Maybe FilePath
                          -> ByteStream m r -> Stream (Of Stretch) m r
decode_v0 nucCode eqs fp = go
  where
    needByte = lift . uncons >=> either (const . lift . throwM $ L.PrematureEOF fp) return

    go = lift . uncons >=> \case
       Left    r     -> pure r
       Right (w,s')
          | w < 0x80 -> let (y,x) = w `divMod` 11 in Chrs (nucCode x) (nucCode y) `cons` go s'
          | w ==0x80 -> Break `cons` go s'

          | w < 0xA0 -> Ns (fromIntegral (w .&. 0x1f)) `cons` go s'
          | w < 0xB0 -> do (w1,s1) <- needByte s'
                           Ns (fromIntegral (w .&. 0x0f) .|.
                               fromIntegral w1 `shiftL` 4) `cons` go s1
          | w < 0xB8 -> do (w1,s1) <- needByte s'
                           (w2,s2) <- needByte s1
                           Ns (fromIntegral (w .&. 0x07) .|.
                               fromIntegral w1 `shiftL` 3 .|.
                               fromIntegral w2 `shiftL` 11) `cons` go s2
          | w < 0xBC -> do (w1,s1) <- needByte s'
                           (w2,s2) <- needByte s1
                           (w3,s3) <- needByte s2
                           Ns (fromIntegral (w .&. 0x03) .|.
                               fromIntegral w1 `shiftL` 2 .|.
                               fromIntegral w2 `shiftL` 10 .|.
                               fromIntegral w3 `shiftL` 18) `cons` go s3
          | w < 0xBE -> do (w1,s1) <- needByte s'
                           (w2,s2) <- needByte s1
                           (w3,s3) <- needByte s2
                           (w4,s4) <- needByte s3
                           Ns (fromIntegral (w .&. 0x01) .|.
                               fromIntegral w1 `shiftL` 1 .|.
                               fromIntegral w2 `shiftL` 9 .|.
                               fromIntegral w3 `shiftL` 17 .|.
                               fromIntegral w4 `shiftL` 25) `cons` go s4
          | w < 0xC0 -> lift . throwM $ L.UnexpectedCode fp w

          | w < 0xE0 -> eqs (fromIntegral (w .&. 0x1f)) `cons` go s'
          | w < 0xF0 -> do (w1,s1) <- needByte s'
                           eqs (fromIntegral (w .&. 0x0f) .|.
                                fromIntegral w1 `shiftL` 4) `cons` go s1
          | w < 0xF8 -> do (w1,s1) <- needByte s'
                           (w2,s2) <- needByte s1
                           eqs (fromIntegral (w .&. 0x07) .|.
                                fromIntegral w1 `shiftL` 3 .|.
                                fromIntegral w2 `shiftL` 11) `cons` go s2
          | otherwise -> lift . throwM $ L.UnexpectedCode fp w


legacy_options :: Parser (LIO ())
legacy_options = legacy_main
    <$> many (strArgument (metavar "HEF_FILE"))
    <*> switch              (short 'k' <> long "keep"                        <> help "Keep (don't delete) input files")
    <*> switch              (short 'f' <> long "force"                       <> help "Convert even if it makes no sense")
    <*> strOption           (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)")
    <*> switch              (short 'R' <> long "recursive"                   <> help "Operate recursively on directories")
    <*> optional (strOption (short 'o' <> long "output"    <> metavar "FILE" <> help "Write output to FILE"))
  where
    legacy_main :: [FilePath] -> Bool -> Bool -> FilePath -> Bool -> Maybe FilePath -> LIO ()
    legacy_main hefs0 conf_keep conf_force conf_reference conf_recursive conf_output = do
        reference <- liftIO $ openTwoBit conf_reference
        hefs <- liftIO $ if conf_recursive then scan_fs hefs0 else return hefs0

        let write fp s = S.writeFile fp $ S.gzip $ L.encode reference s
            conv k fp = streamInput fp $ \istream -> do
                decode_legacy reference (Just fp) (S.gunzip istream) >>= \case
                    (Legacy,  s)        -> do logStringLn $ fp ++ ": converting from legacy format" ; k s
                    (_, s) | conf_force -> do logStringLn $ fp ++ ": converting because of --force" ; k s
                    (Unknown, _)        ->    logStringLn $ fp ++ ": looks like junk, use --force to convert anyway"
                    (Modern,  _)        ->    logStringLn $ fp ++ ": looks like a modern file, no conversion needed"

        case conf_output of
            Just ofp ->
                case hefs of
                    [ifp] -> conv (write ofp) ifp
                    _     -> liftIO . throwIO $ PanicCall "--output requires exactly one input file"

            Nothing  -> forM_ hefs $ \fp -> conv (go fp) fp
              where
                go fp s = do when conf_keep . liftIO $ renameFile fp (fp ++ "~")
                             write fp s


-- | Converts old style 'Stretch' to new style 'Lump'.  Unfortunately,
-- this needs a reference.
stretchToLump :: Monad m => TwoBitFile -> Stream (Of Stretch) m r -> Stream (Of L.Lump) m r
stretchToLump nrs = L.normalizeLump . go1 (map (`tbc_fwd_seq` 0) $ toList $ tbf_chroms nrs)
  where
    go1 [     ] l = lift $ effects l
    go1 (r0:rs) l = go r0 l
      where
        go r = lift . next >=> \case
            Right (Chrs a b,k) -> call a (call b (`go` k)) r
            Right (Ns     c,k) -> L.Ns   (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Eqs    c,k) -> L.Eqs2 (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Eqs1   c,k) -> L.Eqs1 (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Break   ,k) -> L.Break                     `cons` go1 rs k
            Left z             -> pure z

    call :: Monad m => Word8 -> (TwoBitSequence -> Stream (Of L.Lump) m r) -> TwoBitSequence -> Stream (Of L.Lump) m r
    call b k r = case unconsRS r of
        Nothing     -> lift $ Q.effects (k r)
        Just (a,r') -> L.encTwoNucs a (nc2vs U.! fromIntegral b) `Q.cons` k r'

    nc2vs :: U.Vector Word8
    !nc2vs = U.fromList [ 255,  5, 10, 15, 0,        -- NACGT
                            6,  7,  1, 11, 2, 3,     -- MRWSYK
                           17, 18, 19, 16, 255 ]     -- acgtN



data DecodeResult = Legacy  | Modern | Unknown

-- | Legacy decoder.  Switches behavior based on header.  "HEF\0",
-- "HEF\1" are the old format and go through 'Stretch' to 'Lump'
-- conversion;  For the old format, we need a reference, and we have to
-- trust it's a compatible one.  "HEF\3" is the new format.  We could
-- check if the reference is compatible, but chose not to.  This is only
-- used if explicitly requested, and then we might as well trust the
-- user.  ("HEF\2" was an experiment that never left the lab.)
--
-- We try to support ancient files without format marker.  These could
-- start with anything, but in practice start with either (Ns 5000) or
-- (Ns 5001).  Anything else is treated as unknown junk.
decode_legacy :: (MonadIO m, MonadThrow m)
              => TwoBitFile -> Maybe FilePath -> ByteStream m r -> m (DecodeResult, Stream (Of L.Lump) m r)
decode_legacy rs fp strm = do
        hd :> tl <- S.splitAt' 4 strm
        case B.take 3 hd :> B.drop 3 hd of
            "HEF" :> "\0"     -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp tl
            "HEF" :> "\1"     -> return $ (,) Legacy  $ stretchToLump rs $ decode_hap fp tl
            "HEF" :> "\3"     -> return $ (,) Modern  $ L.decodeLump  fp $ S.drop 5 $ S.dropWhile (/= 0) tl
            "\176\113\2" :> _ -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl
            "\177\113\2" :> _ -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl
            _            :> _ -> return $ (,) Unknown $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl


scan_fs :: [FilePath] -> IO [FilePath]
scan_fs [] = return []
scan_fs (x:xs) = do d <- doesDirectoryExist x
                    if d then do ys <- getDirectoryContents x
                                 scan_fs $ [ x ++ "/" ++ y | y <- ys ] ++ xs
                         else (:) x `liftM` scan_fs xs

