-- | We use a 2bit file as reference.  Chromosomes in the file will
-- be used in exactly this order, everything else is ignored.
--
-- When reading input, we always supply the reference file.  Sometimes we
-- don't actually need the reference sequence, but we still use it to
-- decide which chromomes to include, and sometimes how to reorder them.
-- We also compute a checksum over the list of target chromosomes and
-- leave it in the hef files themselves; that way, we can ensure that
-- two hefs can be merged safely.
--
-- Applications for import:
--
--   VCF, BCF files:  A bunch of files, usually in the wrong order.  We
--                    read each, split them into chromosomes, then use
--                    the 2bit file to put them in the correct order.
--
--   BAM files:  Must be sorted, but the order of target sequences is
--               arbitrary.  We require the reference, and reorder valid
--               chromosomes after we're done importing.
--
--   EMF, MAF:  Will not be sorted.  We parse pieces in arbitrary order,
--              and require the reference so we can put everything into
--              a proper order at the end.
--
-- Applications for export:
--
--   D-stats, Treemix:  The reference is no longer needed!
--
--   Eigenstrat:  Can be done without the reference, but will look
--                ridiculous.  Output of proper SNP files requires the
--                reference.
--
--   VCF:  Clearly requires the reference.

module Bio.Heffa.Genome
    ( module Bio.TwoBit
    , RefSeqView(..)
    , dropRS
    , takeRS
    , unconsRS
    , lengthRS
    , viewRS
    , splitMapRS
    , findGenome
    , AlleleCounts(..)
    , I2(..)
    , addAC
    , Var2b(..)
    , Variant(..)
    , isTransversion
    , isKnown
    , hash_ref
    , findChrom
    , filterCpG
    , toRefCode
    , toAltCode
    , appVar
    , tbinfo_options
    , tbtofa_options
    , fatotb_options
    , tbxome_options
    , vcftotb_options
    , buildFasta
    , GffError(..)
    )
    where

import Bio.Prelude                   hiding ( Ns )
import Bio.Streaming                        ( Stream, ByteStream, streamInputs, withOutputFile, concats, inspect )
import Bio.TwoBit
import Control.Monad.Log                    ( PanicCall(..) )
import Data.ByteString.Internal             ( accursedUnutterablePerformIO )
import Data.ByteString.Short                ( ShortByteString, toShort )
import Data.Vector.Storable                 ( Vector )
import Options.Applicative
import Paths_heffalump                      ( getDataDir )
import System.Directory                     ( doesFileExist )
import System.FilePath                      ( takeFileName, takeDirectory, makeRelative, splitSearchPath, (</>) )

import qualified Bio.Streaming.Bytes                as S
import qualified Bio.Streaming.Prelude              as Q
import qualified Data.ByteString                    as B
import qualified Data.ByteString.Builder            as B
import qualified Data.ByteString.Char8              as C
import qualified Data.ByteString.Lazy.Char8         as L
import qualified Data.ByteString.Short              as H
import qualified Data.HashMap.Strict                as M


lengthRS :: TwoBitSequence -> Int
lengthRS = go 0
  where
    go !acc (SomeSeq _ _ _ l s) = go (acc + l) s
    go !acc  RefEnd           = acc
{-# INLINE lengthRS #-}

dropRS :: Int -> TwoBitSequence -> TwoBitSequence
dropRS 0                  s              = s
dropRS _  RefEnd                         = RefEnd
dropRS n (SomeSeq m r o l s) | n > l     = dropRS (n-l) s
                             | n < l     = SomeSeq m r (o + fromIntegral n) (l-n) s
                             | otherwise = s
{-# INLINE dropRS #-}

splitMapRS :: Int -> TwoBitSequence -> (TwoBitSequence -> TwoBitSequence) -> TwoBitSequence
splitMapRS 0  rs                 k             = k rs
splitMapRS _  RefEnd             _             = RefEnd
splitMapRS n (SomeSeq m r o l s) k | n >= l    = SomeSeq m r o l $ splitMapRS (n-l) s k
                                   | otherwise = SomeSeq m r o n $ k $ SomeSeq m r (o + fromIntegral n) (l-n) s
{-# INLINE splitMapRS #-}

takeRS :: Int -> TwoBitSequence -> TwoBitSequence
takeRS n rs = splitMapRS n rs (const RefEnd)
{-# INLINE takeRS #-}

-- | Variant in 2bit encoding: [0..3] for "IOPX" (Identity, transitiOn,
-- comPlement, X-compl)
newtype Var2b = V2b { unV2b :: Word8 } deriving Eq

isTransversion :: Var2b -> Bool
isTransversion (V2b v) = testBit v 1

-- | Applies a variant to a nucleotide, resulting in a new nucleotide.
appVar :: Nuc2b -> Var2b -> Nuc2b
appVar (N2b r) (V2b a) = N2b $ xor r a

-- | Encodes the alternative allele as text: \'.\' for unknown, \"TCAG\" if
-- known, and \"IOPX\" if the type of variant (Identity, transitiOn,
-- comPlement, Xcomplement) is known but the reference isn't.
toAltCode :: Var2b -> Nuc2b -> Char
toAltCode (V2b v) (N2b r)
    | v >= 4    = '.'
    | r >= 4    = C.index "IOPX" $ fromIntegral (xor r v .&. 3)
    | otherwise = C.index "TCAG" $ fromIntegral (xor r v)

-- | Encodes the reference allele as text, one of "TCAGN".
toRefCode :: Nuc2b -> Char
toRefCode (N2b r) | r >= 4 = 'N'
toRefCode (N2b r)          = C.index "TCAG" $ fromIntegral r

isKnown :: Nuc2b -> Bool
isKnown (N2b r) = r < 4

instance Show Var2b where
    show (V2b 0) = "I"
    show (V2b 1) = "O"
    show (V2b 2) = "P"
    show (V2b 3) = "X"
    show      _  = "?"

unconsRS :: TwoBitSequence -> Maybe ( Nuc2b, TwoBitSequence )
unconsRS  RefEnd                                         =  Nothing
unconsRS (SomeSeq msk raw off len s) | isHardMasked msk  =  Just (N2b 255, s')
                                     | otherwise         =  Just (   c,    s')
  where
    c  = N2b $ (accursedUnutterablePerformIO (withForeignPtr raw (flip peekByteOff (fromIntegral $ off `shiftR` 2)))
            `shiftR` (6 - 2 * (fromIntegral $ off .&. 3))) .&. 3
    s' = if len == 1 then s else SomeSeq msk raw (off+1) (len-1) s
{-# INLINE unconsRS #-}


-- | A way to carve up a reference sequence.  Actual references
-- (2bit files) will not have gaps, but the references implicit
-- in Emf and Maf files can.
data RefSeqView a = !Int :== a   -- ^ a stretch of Ns
                  | !Int :-- a   -- ^ a gap
                  | !Nuc2b :^ a  -- ^ a nucleotide
                  | NilRef       -- ^ the end

viewRS :: TwoBitSequence -> RefSeqView TwoBitSequence
viewRS  RefEnd                                                =  NilRef
viewRS (SomeSeq      msk  raw  off len s) | isHardMasked msk  =  len :== s
                                          | otherwise         =   c   :^  s'
  where
    c  = N2b $ (accursedUnutterablePerformIO (withForeignPtr raw (flip peekByteOff (fromIntegral $ off `shiftR` 2)))
            `shiftR` (6 - 2 * (fromIntegral $ off .&. 3))) .&. 3
    s' = if len == 1 then s else SomeSeq msk raw (off+1) (len-1) s

data AlleleCounts = AC { ac_num_ref :: !Word8, ac_num_alt :: !Word8 } deriving Eq

-- Compact storage:  both counts fit into one 'Word8'.
instance Storable AlleleCounts where
    sizeOf    _ = 1
    alignment _ = 1
    poke p (AC r a) = poke (castPtr p) (r + shiftL a 2)
    peek p = (\w -> AC (w .&. 3) (shiftR w 2)) <$> peek (castPtr p)

instance Show AlleleCounts where
    showsPrec _ (AC a b) = shows a . (:) '/' . shows b

-- | A strict pair of 'Int's.  Comparable to 'AlleleCounts', but not
-- prone to overflow if we tally over large (>85) numbers of
-- individuals.  Still looking for a better name.
data I2 = I2 !Int !Int

addAC :: I2 -> AlleleCounts -> I2
addAC (I2 a b) (AC u v) = I2 (a + fromIntegral u) (b + fromIntegral v)

instance Storable I2 where
    sizeOf    ~(I2 a b) = sizeOf a + sizeOf b
    alignment ~(I2 a _) = alignment a
    poke     p (I2 a b) = pokeElemOff (castPtr p) 0 a >> pokeElemOff (castPtr p) 1 b
    peek     p   = I2 <$> peekElemOff (castPtr p) 0  <*> peekElemOff (castPtr p) 1


-- A variant call.  Has a genomic position, ref and alt alleles, and a
-- bunch of calls expressed as allele counts.
data Variant = Variant { v_chr   :: !Int                -- chromosome number
                       , v_pos   :: !Int                -- 0-based
                       , v_ref   :: !Nuc2b
                       , v_alt   :: !Var2b
                       , v_calls :: !(Vector AlleleCounts) }
  deriving Show

-- | Removes variants overlapping a CpG site.  A site is considered a
-- CpG iff the reference has the CpG motif; whether any sample has a
-- variant that produces the CpG is not considered.
filterCpG :: MonadThrow m => TwoBitFile -> Stream (Of Variant) m r -> Stream (Of Variant) m r
filterCpG nrs = go (map (`tbc_fwd_seq` 0) $ toList $ tbf_chroms nrs) 0
  where
    -- We prepend one N to the reference so we can see
    -- if we're on the G of a CpG.
    go [    ] !_ = lift . Q.effects
    go (r:rs) !c = go1 rs (consN r) c 0

    consN    RefEnd             = RefEnd
    consN s@(SomeSeq _ p o _ _) = SomeSeq hardMasked p o 1 s

    go1 rs r !c !p = lift . Q.next >=> \case
        Left x -> pure x
        Right (v,vs)
            | v_chr v > c                -> go rs (succ c) (Q.cons v vs)
            | v_chr v < c || v_pos v < p -> lift . throwM $ PanicCall "[filterCpG] variants should be sorted."
            | otherwise                  -> maybe (k (pure ()) RefEnd) (uncurry k) isCpG
          where
            k f r' = f >> go1 rs r' c (succ p) vs
            isCpG = do
                (x,r')  <- unconsRS $ dropRS (v_pos v - p) r
                (y,r'') <- unconsRS r'
                (z,_)   <- unconsRS r''
                let f | x == N2b 1 && y == N2b 3 = pure ()
                      | y == N2b 1 && z == N2b 3 = pure ()
                      | otherwise                = Q.yield v
                return (f,r')

tbinfo_options :: Parser (LIO ())
tbinfo_options = liftIO . mapM_ go <$> some (strArgument (metavar "2BIT-FILE"))
  where
    go f = do ref <- openTwoBit f
              mapM_ (\TBC{..} -> printf "%s\t%d\n" (C.unpack tbc_name) tbc_dna_size) (tbf_chroms ref)

tbtofa_options :: Parser (LIO ())
tbtofa_options = go <$> strArgument (metavar "2BIT-FILE") <*> many (argument rng (metavar "RANGE"))
  where
    rng = do s0 <- str
             if all (/=':') s0
               then pure (s0, Nothing)
               else do (ch,   ':':s1)     <- pure $ break (':' ==) s0
                       (start,'-':s2) : _ <- pure $ reads s1
                       if null s2
                         then pure (ch, Just (start, Nothing))
                         else do (end,  "")     : _ <- pure $ reads s2
                                 pure (ch, Just (start,Just end))

    go fp [ ] = liftIO $ do
        ref <- openTwoBit fp
        forM_ (tbf_chroms ref) $ \TBC{..} ->
            putStrLn ('>' : C.unpack tbc_name) >> twoBitToFa maxBound (tbc_fwd_seq 0)

    go fp rns = liftIO $ do
        ref <- openTwoBit fp
        forM_ rns $ \(ch,se) ->
            case (findChrom (C.pack ch) ref, se) of
                (Just tbs, Nothing)               -> do printf ">%s\n" ch
                                                        twoBitToFa maxBound $ tbc_fwd_seq tbs 0

                (Just tbs, Just (start,Nothing))  -> do printf ">%s:%d-\n" ch start
                                                        twoBitToFa maxBound $ tbc_fwd_seq tbs start

                (Just tbs, Just (start,Just end)) -> do printf ">%s:%d-%d\n" ch start end
                                                        if start <= end
                                                          then twoBitToFa (end-start) $ tbc_fwd_seq tbs start
                                                          else twoBitToFa (start-end) $ tbc_rev_seq tbs start

                (Nothing, _)                      -> throwIO $ PanicCall $ "Unknown target: " ++ ch


twoBitToFa :: Int -> TwoBitSequence' dir -> IO ()
twoBitToFa ln = Q.mapM_ (B.hPutBuilder stdout) . void . buildFasta 50 . Q.each . take ln . unpackRSMasked

tbxome_options :: Parser (LIO ())
tbxome_options = go <$> strOption (short 'o' <> long "output" <> metavar "FILE" <> value "-" <> help "Write output to FILE")
                    <*> strArgument (metavar "2BIT-FILE")
                    <*> many (strArgument (metavar "GFF-File"))
  where
    go :: FilePath -> FilePath -> [FilePath] -> LIO ()
    go ofile tbf gffs = do
        ref <- liftIO $ openTwoBit tbf
        withOutputFile ofile $ \h ->
            streamInputs gffs $
                Q.mapM_ (liftIO . B.hPutBuilder h) . concats .
                Q.subst (formatCdna ref) . concats . Q.maps (parseAnno . S.lines')


fatotb_options :: Parser (LIO ())
fatotb_options = go
    <$> many (strArgument (metavar "FASTA-FILE"))
    <*> strOption (short 'o' <> long "output" <> metavar "FILE" <> value "-" <> help "Write output to FILE")
  where
    go fs fp = streamInputs fs (faToTwoBit . S.concat) >>=
               liftIO . withOutputFile fp . flip B.hPutBuilder

vcftotb_options :: Parser (LIO ())
vcftotb_options = go
    <$> many (strArgument (metavar "VCF-FILE"))
    <*> strOption (short 'o' <> long "output" <> metavar "FILE" <> value "-" <> help "Write output to FILE")
  where
    go fs fp = streamInputs fs (vcfToTwoBit . S.lines' . S.concat) >>=
               liftIO . withOutputFile fp . flip B.hPutBuilder

-- List of pairs of 'Word32's.  Specialized and unpacked to conserve space.  Probably overkill...
data L2i = L2i {-# UNPACK #-} !Word32 {-# UNPACK #-} !Word32 L2i | L2i_Nil

sum_L2i :: Word32 -> Pair Word32 L2i -> Word32
sum_L2i p (q :!: xs) = go (if q == maxBound then 0 else p-q) xs
  where
    go !a (L2i x y z) = go (a+y-x) z
    go !a  L2i_Nil    = a

encodeL2i :: L2i -> B.Builder
encodeL2i = go 0 mempty mempty
  where
    go !n ss ls  L2i_Nil     = B.word32LE n <> ss <> ls
    go !n ss ls (L2i s e rs) = go (succ n) (B.word32LE s <> ss) (B.word32LE (e-s) <> ls) rs


seqs_to_twobit :: [(ShortByteString, LazyBytes)] -> B.Builder
seqs_to_twobit seqs = B.word32LE 0x1A412743 <> B.word32LE 0 <>
                      B.word32LE (fromIntegral $ length seqs) <> B.word32LE 0 <>
                      fold (zipWith (\nm off -> B.word8 (fromIntegral (H.length nm)) <>
                                                B.shortByteString nm <>
                                                B.word32LE (fromIntegral off))
                                    (map fst seqs) offsets) <>
                      foldMap (B.lazyByteString . snd) seqs
  where
    offset0 = 16 + 5 * length seqs + sum (map (H.length . fst) seqs)
    offsets = scanl (\a b -> a + fromIntegral (L.length b)) offset0 $ map snd seqs



-- Strategy:  We can only write the packedDNA after we wrote the nBlocks
-- and mBlocks.  So packedDNA needs to be buffered.  We have to do three
-- simultaneous strict folds of the input, all of which result in reasonably
-- compact structures, which get concatenated at the end.
--
-- We also have to buffer everything, since the header with the sequence
-- names must be written first.  Oh joy.

faToTwoBit :: (MonadIO m, MonadLog m) => ByteStream m r -> m B.Builder
faToTwoBit = liftM seqs_to_twobit . get_each [] . S.gunzip
  where
    get_each acc = S.uncons . S.dropWhile (/= (c2w '>')) >=> \case
                            Left    _    -> return $ reverse acc
                            Right (_,s2) -> do
                                nm :> s' <- S.toStrict $ S.break (<= 32) s2
                                get_one acc (toShort nm) 0 (maxBound :!: L2i_Nil)
                                        (maxBound :!: L2i_Nil) (0 :!: 0 :!: emptyAccu)
                                        (S.dropWhile (/= (c2w '\n')) s')

    get_one acc !nm !pos !ns !ms !bs = S.uncons >=> \case
        Left    r       -> fin (pure r)
        Right (c,s')
            | c <= 32      -> get_one acc nm pos ns ms bs s'
            | c == c2w '>' -> fin (S.cons c s')
            | otherwise    -> get_one acc nm (succ pos)
                                      (collect_Ns ns pos c)
                                      (collect_ms ms pos $ w2c c)
                                      (collect_bases bs $ w2c c) s'
      where
        fin k = do let !r = encode_seq pos ns ms bs
                   logStringLn $ printf "%s: %d kbases (%d Ns), %dkB encoded"
                                 (show nm) (pos `div` 1000) (sum_L2i pos ns) (L.length r `div` 1024)
                   get_each ((nm,r):acc) k


-- | Extracts the reference from a VCF.  This assumes the presence of at
-- least one record per site.  The VCF must be sorted by position.  When
-- writing out, we try to match the order of the contigs as listed in
-- the header.  Unlisted contigs follow at the end with their order
-- preserved; contigs without data are not written at all.
vcfToTwoBit :: (MonadIO m, MonadLog m) => Stream (Of Bytes) m r -> m B.Builder
vcfToTwoBit s0 = do lns :> s1 <- read_header [] s0
                    seqs <- get_each lns [] $
                        Q.filter (\s -> not (B.null s) && C.head s /= '#') s1
                    return $ seqs_to_twobit $ reorder (map fst lns) seqs
  where
    -- Collects the "contig" stanzas, parses their lengths.  Returns the
    -- length map and the remaining stream.
    read_header acc = Q.next >=> \case
        Left r                                               -> return $ reverse acc :> pure r
        Right (l,ls) | "##contig=" `C.isPrefixOf` l
                     , (Just !nm, Just !ln) <- parse_cline l -> read_header ((nm,ln):acc) ls
                     | "#" `C.isPrefixOf` l                  -> read_header acc ls
                     | otherwise                             -> return $ reverse acc :> Q.cons l ls

    parse_cline = p1 . C.filter (not . isSpace) . C.takeWhile (/='>') . C.drop 1 . C.dropWhile (/='<')
      where
        p1 s | "ID=" `C.isPrefixOf` s = let (nm,t) = C.break (==',') $ C.drop 3 s
                                            (_,ln) = p1 $ C.drop 1 t
                                        in (Just (toShort nm),ln)

             | "length=" `C.isPrefixOf` s = case C.readInt $ C.drop 7 s of
                    Just (ln,u) -> let (nm,_) = p1 $ C.drop 1 $ C.dropWhile (/=',') u in (nm,Just (fromIntegral ln))
                    Nothing     -> p1 $ C.drop 1 $ C.dropWhile (/=',') s

             | C.null s = (Nothing,Nothing)
             | otherwise = p1 $ C.drop 1 $ C.dropWhile (/=',') s

    get_each :: (MonadIO m, MonadLog m)
             => [(ShortByteString,Word32)]
             -> [(ShortByteString, LazyBytes)]
             -> Stream (Of Bytes) m r
             -> m [(ShortByteString, LazyBytes)]
    get_each lns acc = Q.next >=> \case
                            Left    _    -> return $ reverse acc
                            Right (l,s2) -> do
                                let nm = B.takeWhile (/= 9) l
                                (pos,ns,bs) :> s3 <- get_one nm 0 (maxBound :!: L2i_Nil)
                                                             (0 :!: 0 :!: emptyAccu) (Q.cons l s2)
                                let !nm' = toShort nm
                                    (ns',bs',ln') =
                                        case find ((==) nm' . fst) lns of
                                            Just (_,ln) | ln > pos ->
                                                 (extend_gap ns ln, pad_bases bs (fromIntegral $ ln-pos), ln)
                                            _ -> (ns,bs,pos)
                                    !r = encode_seq ln' ns' (maxBound :!: L2i_Nil) bs'

                                logStringLn $ printf "%s: %d kbases (%d Ns), %dkB encoded"
                                              (show nm') (ln' `div` 1000) (sum_L2i ln' ns') (L.length r `div` 1024)

                                L.length r `seq` get_each lns ((nm',r):acc) s3

    -- important: 1-based coordinates!
    get_one !nm !pos !ns !bs = Q.next >=> \case
        Left    r                           -> return $ (pos,ns,bs) :> pure r
        Right (l,s')
            | B.takeWhile (/=9) l /= nm     -> return $ (pos,ns,bs) :> Q.cons l s'

            | Just (pos',l3) <- C.readInt . C.drop 1 $ B.dropWhile (/=9) l
            , ref <- B.takeWhile (/=9) . B.drop 1 . B.dropWhile (/=9) $ B.drop 1 l3
            , fromIntegral pos' >= pos + 1
            , not (C.null ref) ->
                if fromIntegral pos' == pos + 1
                    -- record in sequence
                    then get_one nm (succ pos) (collect_Ns ns pos $ B.head ref)
                                               (collect_bases bs  $ C.head ref) s'
                    -- gap:  handle the gap, reprocess the record
                    else let gap_len = pos' - fromIntegral pos - 1
                         in get_one nm (fromIntegral pos' - 1) (extend_gap ns pos)
                                       (pad_bases bs gap_len) (Q.cons l s')

            -- anything else can be ignored (parse errors or additional records)
            | otherwise                     -> get_one nm pos ns bs s'


    pad_bases bs n = foldl' collect_bases bs $ replicate n 'T'

    -- Reorder a key-value list so it matches the order of a list of
    -- keys.  Missing keys are ignored, leftover pairs retain their
    -- original order.
    reorder :: Eq a => [a] -> [(a,b)] -> [(a,b)]
    reorder [    ] vs = vs
    reorder (k:ks) vs = go [] vs
      where
        go xs ((k1,v1):ys) | k  ==  k1 = (k1,v1) : reorder ks (reverse xs ++ ys)
                           | otherwise = go ((k1,v1):xs) ys
        go xs [          ]             = reorder ks (reverse xs)


encode_seq :: Word32                                    -- ^ length
           -> Pair Word32 L2i                           -- ^ list of N stretches
           -> Pair Word32 L2i                           -- ^ list of mask stretches
           -> Pair (Pair Int Word8) Accu                -- ^ accumulated bases
           -> LazyBytes

encode_seq pos ns ms bs = L.length r `seq` r
  where
    ss' = case bs of (0 :!: _ :!: ss) -> ss
                     (n :!: w :!: ss) -> grow (w `shiftL` (8-2*n)) ss
    r = B.toLazyByteString $
              B.word32LE pos <>
              encodeL2i (case ns of p :!: rs | p == maxBound -> rs ; p :!: rs -> L2i p pos rs) <>
              encodeL2i (case ms of p :!: rs | p == maxBound -> rs ; p :!: rs -> L2i p pos rs) <>
              B.word32LE 0 <>
              buildAccu ss'


-- | Collects stretches of Ns by looking at one character at a time.  In
-- reality, anything that isn't one of \"ACGT\" is treated as an N.
collect_Ns :: Pair Word32 L2i -> Word32 -> Word8 -> Pair Word32 L2i
collect_Ns (spos :!: rs) pos c
    | spos == maxBound && c `B.elem` "ACGTacgt" = maxBound :!: rs
    | spos == maxBound                          =      pos :!: rs
    |                     c `B.elem` "ACGTacgt" = maxBound :!: L2i spos pos rs
    | otherwise                                 =     spos :!: rs

extend_gap :: Pair Word32 L2i -> Word32 -> Pair Word32 L2i
extend_gap (spos :!: rs) pos
    | spos == maxBound                          =      pos :!: rs
    | otherwise                                 =     spos :!: rs

-- | Collects stretches of masked dna by looking at one letter at a
-- time.  Anything lowercase is considered masked.
collect_ms :: Pair Word32 L2i -> Word32 -> Char -> Pair Word32 L2i
collect_ms (spos :!: rs) pos c
    | spos == maxBound && isUpper c = maxBound :!: rs
    | spos == maxBound              =      pos :!: rs
    |                     isUpper c = maxBound :!: L2i spos pos rs
    | otherwise                     =     spos :!: rs

-- | Collects bases in 2bit format.  It accumulates 4 bases in one word,
-- then collects bytes in an 'Accu'.  From the 2bit spec:
--
-- packedDna - the DNA packed to two bits per base, represented as
--             so: T - 00, C - 01, A - 10, G - 11. The first base is
--             in the most significant 2-bit byte; the last base is
--             in the least significant 2 bits. For example, the
--             sequence TCAG is represented as 00011011.
collect_bases :: Pair (Pair Int Word8) Accu -> Char -> Pair (Pair Int Word8) Accu
collect_bases (n :!: w :!: ss) c
    = let code = case c of 'C'->1;'c'->1;'A'->2;'a'->2;'G'->3;'g'->3;_->0
          w'   = shiftL w 2 .|. code
      in if n == 3 then 0 :!: 0 :!: grow w' ss else succ n :!: w' :!: ss

-- | A way to accumulate bytes.  If the accumulated bytes will hang
-- around in memory, this has much lower overhead than 'Builder'.  If it
-- has short lifetime, 'Builder' is much more convenient.
newtype Accu = Accu [Bytes]

emptyAccu :: Accu
emptyAccu = Accu []

-- | Appends bytes to a collection of 'Bytes' in such a way that the
-- 'Bytes' keep doubling in size.  This ensures O(n) time and space
-- complexity and fairly low overhead.
grow :: Word8 -> Accu -> Accu
grow w = go 1 [B.singleton w]
  where
    go l acc (Accu (s:ss))
        | B.length s <= l  = go (l+B.length s) (s:acc) (Accu ss)
        | otherwise        = let !s' = B.concat acc in  Accu (s' : s : ss)
    go _ acc (Accu [    ]) = let !s' = B.concat acc in  Accu [s']

buildAccu :: Accu -> B.Builder
buildAccu (Accu ss) = foldMap B.byteString $ reverse ss


-- | Computes a hash value over chromosome names and lengths.  This is
-- useful to fingerprint a reference genome and make sure it is
-- compatible with a given heffalump.  This computes a 64-bit FNV-1 hash
-- and returns the lower 32 bits.

hash_ref :: TwoBitFile -> Word32
hash_ref g = fromIntegral . (.&.) 0xFFFFFFFF
           $ hash_list fnv1 (fmap tbc_dna_size $ tbf_chroms g)
           $ hash_list (B.foldl' fnv1) (fmap tbc_name $ tbf_chroms g)
           $ (0xdc36d1615b7400a4 :: Word64)
  where
    hash_list  h ar salt = finalise $ foldl' (step h) (salt :!: 0) ar
    step   h (s :!: l) x = h s x :!: succ l
    finalise (s :!: l)   = fnv1 s (l::Int)
    fnv1           h1 h2 = (h1 * 16777619) `xor` fromIntegral h2


-- | Tries to find a genome.  Each pair in the argument is the path to a
-- file whose parent directories should be searched and the name of the
-- file to look for.
findGenome :: [( FilePath, FilePath )] -> IO (Either String TwoBitFile)
findGenome files = do
    ddir <- getDataDir
    epth <- (++ [ddir]) . maybe [] splitSearchPath <$> lookupEnv "HEFFALUMP"

    let look = nubHash $ concatMap (look1 epth) files

    filterM doesFileExist look >>= \case
        fp : _ -> Right <$> openTwoBit fp
        [    ] -> return . Left $ "No reference found.  Looked for it at "
                    ++ intercalate ", " look ++ "."
  where
    -- List of names to look at:  the literal name, relative to the
    -- input file; the file name in any directory of the search path;
    -- the file name in any parent directory of the input file.
    look1 epth (hef, rname) =
        makeRelative hef rname :
        map (</> takeFileName rname) (epth ++ parents hef)

    parents f = let d = takeDirectory f in if d == f then [] else d : parents d



data Gene = Gene { g_id :: Bytes, g_symbol :: Bytes, g_biotype :: Bytes }

null_gene :: Gene
null_gene = Gene "" "" ""


-- | A cDNA or mRNA or transcript (these are all synonymous), with some
-- metainformation collected from the annotation.  Whatever the input
-- was called, we call it 'cdna' in the transciptome.
data Cdna = Cdna
    { c_id           :: Bytes           -- identifier (typically an ENST number)
    , c_pos          :: Range           -- genomic position
    , c_gene_id      :: Bytes           -- gene identifier (typically an ENSG number)
    , c_gene_symbol  :: Bytes           -- colloquial name, aka locus
    , c_gene_biotype :: Bytes           -- whatever, just pass it on
    , c_biotype      :: Bytes           -- whatever, just pass it on
    , c_description  :: Bytes           -- unclear; always empty for now
    , c_exons        :: [Range]         -- list of exon coordinates (sorted backwards)
    }
  deriving Show

null_cdna :: Cdna
null_cdna = Cdna "" (Range (Pos "" 0) 0) "" "" "" "" "" []


data GffError = GffParseError Int | GffIdMismatch Int | GffUnknownRef Bytes deriving Show

instance Exception GffError where
    displayException (GffParseError ln) = "parse error in line " ++ show ln ++ " of gff"
    displayException (GffIdMismatch ln) = "identifier does not match in line " ++ show ln ++ " of gff"
    displayException (GffUnknownRef ch) = "unknown reference " ++ show ch

-- | Parses annotations in GFF format.  We want to turn an annotation
-- and a 2bit file into a FastA of the transcriptome (one sequence per
-- annotated transcript), that looks like the stuff Lior Pachter feeds
-- into Kallisto.  Annotations come in two dialects of GFF, either GFF3
-- or GTF.  We autodetect and understand both.

parseAnno :: MonadLog m => Stream (Of Bytes) m r -> Stream (Of Cdna) m r
parseAnno = Q.filter (not . null . c_exons) .
            go null_gene null_cdna .
            Q.map (second (C.split '\t')) .
            Q.filter (\(_,s) -> not (B.null s) && C.head s /= '#') .
            Q.zip (Q.enumFrom 1)
  where
    go gene xscript =
        lift . inspect >=> \case
            Left r -> Q.yield xscript >> pure r
            Right ((ln, ch:_:tp:fro_:tho_:_:strand:_:stuff_:_) :> strm)
                | Just (fro,"") <- C.readInt fro_
                , Just (tho,"") <- C.readInt tho_
                , Just stuff <- parseStuff stuff_ ->
                    let rng = bool id reverseRange (strand == "-") $ Range (Pos ch (fro-1)) (tho-fro+1)
                    in go2 ln gene xscript strm rng (B.map (.|. 32) tp) stuff
            Right ((ln, _) :> strm) -> do lift $ logMsg Warning $ GffParseError ln
                                          go gene xscript strm

    go2 ln gene xscript strm rng tp (Left stuff)
        | tp == "exon" = if M.lookup "Parent" stuff == Just (c_id xscript)
                         then go gene xscript { c_exons = rng : c_exons xscript } strm
                         else do lift $ logMsg Warning $ GffIdMismatch ln
                                 go gene xscript strm

        | tp == "transcript" || tp == "cdna" || tp == "mrna" = do
                Q.yield xscript
                case (M.lookup "ID" stuff, M.lookup "Parent" stuff) of
                    (Just tid, Just gid)
                        | gid == g_id gene -> let xscript' = Cdna { c_id = tid
                                                                  , c_pos = rng
                                                                  , c_gene_id = gid
                                                                  , c_gene_symbol = g_symbol gene
                                                                  , c_gene_biotype = g_biotype gene
                                                                  , c_biotype = M.lookupDefault "" "biotype" stuff
                                                                  , c_description = "" -- XXX
                                                                  , c_exons = [] }
                                              in go gene xscript' strm

                        | otherwise -> do lift $ logMsg Warning $ GffIdMismatch ln
                                          go gene null_cdna strm

                    _ -> do lift $ logMsg Warning $ GffParseError ln
                            go gene null_cdna strm

        | tp == "gene" = do
                Q.yield xscript
                case M.lookup "ID" stuff of
                    Just gid -> let gene' = Gene { g_id = gid
                                                 , g_symbol = ""    -- XXX
                                                 , g_biotype = M.lookupDefault "" "biotype" stuff }
                                in go gene' null_cdna strm

                    Nothing -> do lift $ logMsg Warning $ GffParseError ln
                                  go null_gene null_cdna strm

        | otherwise = go gene xscript strm

    go2 ln gene xscript strm rng tp (Right stuff)
        | tp == "exon" = do
                case M.lookup "transcript_id" stuff of
                    Just tid
                        | tid == c_id xscript -> go gene xscript { c_exons = rng : c_exons xscript } strm

                        | otherwise -> do lift $ logMsg Warning $ GffIdMismatch ln
                                          go gene xscript strm

                    Nothing -> do lift $ logMsg Warning $ GffParseError ln
                                  go gene xscript strm


        | tp == "transcript" || tp == "cdna" || tp == "mrna" = do
                Q.yield xscript
                case (M.lookup "transcript_id" stuff, M.lookup "gene_id" stuff) of
                    (Just tid, Just gid) -> let xscript' = Cdna { c_id = tid
                                                                , c_pos = rng
                                                                , c_gene_id = gid
                                                                , c_gene_symbol = M.lookupDefault "" "gene_name" stuff
                                                                , c_gene_biotype = ""   -- XXX
                                                                , c_biotype = "" -- XXX
                                                                , c_description = "" -- XXX
                                                                , c_exons = [] }
                                            in go gene xscript' strm

                    _ -> do lift $ logMsg Warning $ GffParseError ln
                            go gene null_cdna strm

        | otherwise = go gene xscript strm


-- | Parses the random stuff in GFF into a hash table.  Returns 'Just
-- (Left _)' if the file uses assignment style ("foo=bar;"), returns
-- 'Just (Right _)' if the file uses statement style ("foo \"bar\";"),
-- otherwise returns Nothing.
parseStuff :: Bytes -> Maybe (Either (M.HashMap Bytes Bytes) (M.HashMap Bytes Bytes))
parseStuff s = Left  <$> parse_assignments M.empty s <|>
               Right <$> parse_quoted M.empty s
  where
    parse_assignments !h s0
        | C.null s0 = Just h
        | otherwise = do let (k,s1) = C.break (=='=') s0
                         guard . not $ C.null k
                         guard . not $ C.null s1
                         let (v,s2) = C.break (==';') $ C.tail s1
                         parse_assignments (M.insert k v h) (C.drop 1 s2)

    parse_quoted !h s0
        | C.null s0 || C.head s0 == '#' = Just h
        | otherwise = do let (k,s1) = C.break (==' ') s0
                         guard . not $ C.null k
                         guard $ C.isPrefixOf " \"" s1
                         let (v,s2) = C.break (=='"') $ C.drop 2 s1
                         guard . not $ C.null s2
                         let s3 = C.dropWhile (/=';') s2
                         parse_quoted (M.insert k v h) . C.dropWhile isSpace $ C.drop 1 s3


formatCdna :: MonadLog m => TwoBitFile -> Cdna -> Stream (Of B.Builder) m ()
formatCdna tbf Cdna{..} = Q.yield descr >> getExons >>= buildFasta 60 . Q.each
  where
    (_,tbf_fn) = C.breakEnd (=='/') $ tbf_path tbf
    (tbf_base,_) = C.breakEnd (=='.') tbf_fn
    genome_id = if C.null tbf_base then tbf_fn else C.init tbf_base

    descr = B.char7 '>' <> B.byteString c_id <> " cdna chromosome:" <>
            B.byteString genome_id <> B.char7 ':' <> formatRange c_pos <>
            " gene:" <> B.byteString c_gene_id <>
            maybeBS " gene_biotype:" c_gene_biotype <>
            maybeBS " transcript_biotype:" c_biotype <>
            maybeBS " gene_symbol:" c_gene_symbol <>
            maybeBS " description:" c_description <> B.char7 '\n'

    formatRange r | p_is_reverse (r_pos r) = formatRange1 (reverseRange r) <> ":-1"
                  | otherwise              = formatRange1 r <> ":1"

    formatRange1 r = B.byteString (p_seq (r_pos r)) <> B.char7 ':' <>
                     B.intDec (p_start (r_pos r)) <> B.char7 ':' <>
                     B.intDec (p_start (r_pos r) + r_length r - 1)

    maybeBS p s = if B.null s then mempty else p <> B.byteString s

    getExons | p_is_reverse (r_pos c_pos)  =  concat <$> mapM getExon c_exons
             | otherwise                   =  concat <$> mapM getExon (reverse c_exons)

    getExon :: MonadLog m => Range -> m [Word8]
    getExon (Range (Pos ch start) len) =
        case findChrom ch tbf of
            Just tbs | start >= 0 -> pure $ take len $ unpackRS $ tbc_fwd_seq tbs start
                     | otherwise  -> pure $ take len $ unpackRS $ tbc_rev_seq tbs (-start-len)
            Nothing               -> [ ] <$ logMsg Warning (GffUnknownRef ch)


buildFasta :: Monad m => Int -> Stream (Of Word8) m r -> Stream (Of B.Builder) m r
buildFasta n = go
  where
    go = lift . Q.next >=> \case
        Left     r  -> pure r
        Right (w,s) -> do
            Q.yield $ B.word8 w
            s' <- Q.map B.word8 $ Q.splitAt (n-1) s
            Q.yield $ B.char7 '\n'
            go s'
{-# INLINE buildFasta #-}

-- | Finds a named scaffold in the reference.  If it doesn't find the
-- exact name, it will try to compensate for the crazy naming
-- differences between NCBI and UCSC.  This doesn't work in general, but
-- is good enough in the common case.  In particular, "1" maps to "chr1"
-- and back, "GL000192.1" to "chr1_gl000192_random" and back, and "chrM"
-- to "MT" and back.
findChrom :: Bytes -> TwoBitFile -> Maybe TwoBitChromosome
findChrom c TBF{ tbf_chrmap = cs } =
    msum [ M.lookup c cs
         , M.lookup ("chr" <> c) cs
         , guard ("chr" `B.isPrefixOf` c) >> M.lookup (B.drop 3 c) cs
         , guard ("chrM" == c) >> M.lookup "MT" cs
         , guard ("MT" == c) >> M.lookup "chrM" cs
         , case filter (\d -> match c (tbc_name d) || match (tbc_name d) c) $ M.elems cs of
                [x] -> Just x ; _ -> Nothing ]
  where
    match x y = C.isInfixOf (C.map toLower (C.takeWhile (/= '.') x)) y

